<?php

declare(strict_types=1);

require_once(__DIR__ . "/../../src/modele/classes/Compte.class.php");

use PHPUnit\Framework\TestCase;

final class CompteTest extends TestCase {

    protected $compte;
    protected $id;
    protected $nom;
    protected $courriel;
    protected $mdp;  //Mot De Passe
    protected $role;

    protected function setUp(): void {
        $this->compte = new Compte();
        $this->id = 'i22ko93iw8u3d';
        $this->nom = "Yohan Gagnon-K";
        $this->courriel = "test51@gmail.com";
        $this->mdp = 'allo123';
        $this->role = 1;
    }

    public function testCreationCompte(): void {
        $this->assertInstanceOf(
                Compte::class,
                $this->compte
        );
    }

    public function testSetId(): void {
        $this->compte->setId('t6784rt');
        $this->assertEquals('t6784rt', $this->compte->getId());
    }

    public function testSetNom(): void {
        $this->compte->setNom('Bob');
        $this->assertEquals('Bob', $this->compte->getNom());
    }

    public function testSetCourriel(): void {
        $this->compte->setCourriel('test1337@gmail.com');
        $this->assertEquals('test1337@gmail.com', $this->compte->getCourriel());
    }

    public function testSetPassword(): void {
        $this->compte->setMdp('allo12345');
        $this->assertEquals('allo12345', $this->compte->getMdp());
    }

    public function testSetRole(): void {
        $this->compte->setRole(0);
        $this->assertEquals(0, $this->compte->getRole());
    }

    public function testHashMdp(): void {
        $this->compte->setMdp('allo12345');
        $this->compte->hasherMdp();
        $this->assertEquals(true, $this->compte->verifierMdp('allo12345'));
    }
}
