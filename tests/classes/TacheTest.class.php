<?php

declare(strict_types=1);

require_once(__DIR__ . "/../../src/modele/classes/Tache.class.php");

use PHPUnit\Framework\TestCase;

final class TacheTest extends TestCase {
    protected $tache;

    protected function setUp(): void {
        $this->tache = new Tache(null);
    }

    public function testCreationTache(): void {
        $this->assertInstanceOf(Tache::class,$this->tache);
    }

    public function testSetId(): void { 
		$this->tache->setId("12345");
		$this->assertEquals("12345", $this->tache->getId());
	}
	
	public function testSetNom(): void { 
		$this->tache->setNom("Pratique");
		$this->assertEquals("Pratique", $this->tache->getNom());
	}
	
	public function testSetDescription(): void { 
		$this->tache->setDescription("Une description");
		$this->assertEquals("Une description", $this->tache->getDescription());
	}
	
	public function testSetDateCreation(): void { 
		$this->tache->setDateCreation("2019-09-09");
		$this->assertEquals("2019-09-09", $this->tache->getDateCreation());
	}
	
	public function testSetEstTermine(): void { 
		$this->tache->setEstTermine(true);
		$this->assertEquals(true, $this->tache->getEstTermine());
	}
	
	public function testSetNbIteration(): void { 
		$this->tache->setNbIteration(5);
		$this->assertEquals(5, $this->tache->getNbIteration());
	}
	
	public function testSetNomFichier(): void { 
		$this->tache->setNomFichier("doc");
		$this->assertEquals("doc", $this->tache->getNomFichier());
	}
	
	public function testSetTypeFichier(): void { 
		$this->tache->setTypeFichier("pdf");
		$this->assertEquals("pdf", $this->tache->getTypeFichier());
	}
	
	public function testSetPathFichier(): void { 
		$this->tache->setPathFichier(".\test");
		$this->assertEquals(".\test", $this->tache->getPathFichier());
	}
}