<?php

declare(strict_types=1);

require_once(__DIR__ . "/../../src/modele/classes/Message.class.php");

use PHPUnit\Framework\TestCase;

final class MessageTest extends TestCase {
    protected $message;
    
    protected function setUp(): void { 
            $this->message = new Message();
    }
    
    public function testCreationMessage(): void {
        $this->assertInstanceOf(Message::class,$this->message);
    }
    
    public function testSetIdMessage(): void {
        $this->message->setId_message('123456789');
        $this->assertEquals('123456789', $this->message->getId_message());
    }
    
    public function testSetIdDestinataire(): void {
        $this->message->setId_destinataire('450607');
        $this->assertEquals('450607', $this->message->getId_destinataire());
    }
    
    public function testSetIdExpediteur(): void {
        $this->message->setId_expediteur('987654');
        $this->assertEquals('987654', $this->message->getId_expediteur());
    }
    
    public function testSetCategorie(): void {
        $this->message->setCategorie(1);
        $this->assertEquals(1, $this->message->getCategorie());
    }
    
    public function testSetEstlu(): void {
        $this->message->setEst_lu(true);
        $this->assertEquals(true, $this->message->getEst_lu());
    }
    
    public function testSetContenu(): void {
        $this->message->setContenu('Bonjour');
        $this->assertEquals('Bonjour', $this->message->getContenu());
    }
    
    public function testSetTitre(): void {
        $this->message->setTitre('Message');
        $this->assertEquals('Message', $this->message->getTitre());
    }
    
    public function testSetDateEnvoi(): void {
        $this->message->setDate_envoi('2019-01-01');
        $this->assertEquals('2019-01-01', $this->message->getDate_envoi());
    }
}
