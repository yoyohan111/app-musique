<div class="container">
    <?php
    if (isset($_REQUEST["message_erreur"])) {
            echo "<div class='alert alert-danger'>";
            echo $_REQUEST["message_erreur"];
            echo "</div>";
    }
    
    if (isset($_REQUEST["global_message"])) {
        $msg = "<span class=\"warningMessage\">" . $_REQUEST["global_message"] . "</span>";
    }
    $u = "";
    if (isset($_REQUEST["courriel"])) {
        $u = $_REQUEST["courriel"];
    }
    ?>
    <h1>Connexion</h1>
    <form action="" method="get">
        <div class="form-group">
            <label for="courriel"><b>Mon adresse Courriel:</b></label>
            <div class="col-sm-8 col-md-6 col-lg-4 col-xl-4" style="padding: 0">
                <input class="form-control" name="courriel" type="email" placeholder="Entrer courriel" value="<?php echo $u ?>" required/>
                <?php
                if (isset($_REQUEST["field_messages"]["courriel"])) {
                    echo "<span class=\"warningMessage\" style='color: red;'>" . $_REQUEST["field_messages"]["courriel"] . "</span>";
                }
                ?>
            </div>
        </div>
        <input name="action" value="connexion" type="hidden" />
        <input class="btn btn-primary" value="ME CONNECTER" type="submit" />
    </form>
</div>
