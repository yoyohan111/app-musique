<div class="container">

    <?php
    if (!isset($_REQUEST['info_type'])) {
        echo "<h2>Il n'y a rien à vous aviser..</h2>";
    ?>
        <script type="text/javascript">
            setTimeout(function() {
                window.location.href = '?action=accueil';
            }, 2500); //Après 2.5 secondes
        </script>
    <?php
    } elseif ($_REQUEST["info_type"] == "deconnexion") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        echo "<h2>Vous êtes déconnecté avec succès.</h2>";
    ?>
        <script type="text/javascript">
            setTimeout(function() {
                window.location.href = '?action=afficherConnexion';
            }, 1300); //Après 1.3 secondes
        </script>
    <?php
    } elseif ($_REQUEST["info_type"] == "ajout_tache_erreur") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h1 style="color: red;">Erreur dans l'ajout de la tâche #<?= $_REQUEST["info_numTache"] ?></h1>
        <h2>Exception reçue: </h2>
        <h4><?= $_REQUEST["info_message"] ?> </h4>
    <?php
    } elseif ($_REQUEST["info_type"] == "ajout_tache_sql") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h1 style="color: red;">ERREUR</h1>
        <h2>Tâche #<?= $_REQUEST["info_numTache"] ?> non enregistré.</h2>
        <h4><?= $_REQUEST["info_message"] ?></h4>
    <?php
    } elseif ($_REQUEST["info_type"] == "ajout_fichier_erreur") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h1 style="color: red;">Erreur dans l'ajout du fichier dans la tâche #<?= $_REQUEST["info_numTache"] ?></h1>
        <h2>Exception reçue: </h2>
        <h4><?= $_REQUEST["info_message"] ?></h4>
    <?php
    } elseif ($_REQUEST["info_type"] == "ajout_travail_erreur") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h1 style="color: red;">Erreur dans l'ajout du travail.</h1>
        <h2>Exception reçue: </h2>
        <h4><?= $_REQUEST["info_message"] ?></h4>
    <?php
    } elseif ($_REQUEST["info_type"] == "ajout_travail") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h1><?= $_REQUEST["info_titre"] ?></h1>
        <script type="text/javascript">
            //redirection avec timer
            setTimeout(function() {
                window.location.href = '?action=AfficherAjoutTravail';
            }, 2000); //Après 2 secondes
        </script>
    <?php
    } elseif ($_REQUEST["info_type"] == "suppr_tache_succes") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h2 style="color: red;">Tâche supprimé.</h2>
        <script type="text/javascript">
            //redirection avec timer
            setTimeout(function() {
                window.location.href = '?action=afficherListeTaches';
            }, 1500); //Après 1.5 secondes
        </script>
    <?php
    } elseif ($_REQUEST["info_type"] == "suppr_tache_erreur") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h2 style="color: red;">Erreur dans la suppression de la tâche!</h2>
    <?php
    } elseif ($_REQUEST["info_type"] == "ajout_compte") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
    <h2><?= $_REQUEST["info_titre"] ?></h2>
    <script type="text/javascript"> //redirection avec timer
        setTimeout(function() {
            window.location.href = '?action=formAjoutCompte';
        }, 1500); //Après 1.5 secondes
    </script>
    <?php
    } elseif ($_REQUEST["info_type"] == "redirection") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h4><?= $_REQUEST["info_message"] ?> </h4>
    <?php
    } elseif ($_REQUEST["info_type"] == "erreur") {
        unset($_REQUEST['info_type']);  //pour laisser place aux prochains messages d'info
        //afficher le message
    ?>
        <h1 style="color: red;">Erreur</h1>
        <h2><?= $_REQUEST["info_titre"] ?></h2>
        <h4><?= $_REQUEST["info_message"] ?> </h4>
    <?php
    }
    ?>

</div>