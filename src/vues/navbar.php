<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" style="padding-top: 0px; padding-bottom: 0px;">
<div class="container">
    <a class="navbar-brand">
    	<img width="150" src="styles/images/Logo-menu.png">
    	</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto" id="myTab">

                <!-- Afficher une barre de menu avec des options différentes relativement au status de connection de l'utilisateur! -->
                <?php
                if (!isset($_SESSION["connecte"])) {    //si l'utilisateur n'est PAS connecté..
                    ?>
                    <li class="nav-item pl-5"></li> <!--  pour rajouter un espace.. -->
                    </ul>
                    <form action="?action=afficherConnexion" method="POST" class="form-inline my-md-0 ml-2">
                        <button class="btn btn-success my-2 my-sm-0" type="submit">Connexion</button>
                    </form>
                <?php
                } else {    //si l'utilisateur EST connecté.. on check son role
                    if ($_SESSION["connecte"]["role"] == 0) {   //si c'est un Admin
                        ?>
                        <li class="nav-item <?php if ($_SESSION['menu']=="tableauBord") {echo "active";} ?>">
                            <a class="nav-link" href="?action=tableauBord">Tableau de bord</a>
                        </li>
                        <li class="nav-item dropdown <?php if ($_SESSION['menu']=="comptes") {echo "active";} ?>">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown_MenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Gestion des comptes
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown_MenuLink">
                                <a class="dropdown-item" href="?action=formAjoutCompte">Ajouter un Professeur</a>
                                <a class="dropdown-item" href="?action=listeUtilisateurs">Liste des utilisateurs</a>
                                <a class="dropdown-item" href="?action=AfficherModifierCompte">Modifier mon compte</a>
                            </div>
                        </li>
                        <?php
                        } elseif ($_SESSION["connecte"]["role"] == 1) {   //si c'est un Prof
                            ?>
                        <li class="nav-item <?php if ($_SESSION['menu'] == "tableauBord") {echo "active";} ?>">
                            <a class="nav-link" href="?action=tableauBord">Tableau de bord</a>
                        </li>
                        <li class="nav-item <?php if ($_SESSION['menu'] == "afficherTravail") {echo "active";} ?>">
                            <a class="nav-link" href="?action=afficherListeTaches">Tâches donnés</a>
                        </li>
                        <li class="nav-item <?php if ($_SESSION['menu'] == "donnerTravail") {echo "active";} ?>">
                            <a class="nav-link" href="?action=AfficherAjoutTravail">+ Tâche</a>
                        </li>
                        <li class="nav-item <?php if ($_SESSION['menu'] == "memo") {echo "active";} ?>">
                            <a class="nav-link" href="?action=formRedactionMessageProf">+ Memo</a>
                        </li>
                        <li class="nav-item dropdown <?php if ($_SESSION['menu']=="comptes") {echo "active";} ?>">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown_MenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Gestion de comptes
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown_MenuLink">
                                <a class="dropdown-item" href="?action=formAjoutCompte">Ajouter un Élève</a>
                                <a class="dropdown-item" href="?action=AfficherModifierCompte">Mon Compte</a>
                            </div>
                        </li>
                        <?php
                        } elseif ($_SESSION["connecte"]["role"] == 2) { //si c'est un Élève
                            ?>
                            <li class="nav-item <?php
                                if ($_SESSION['menu'] == "tableauBord") {echo "active";} ?>">
                                <a class="nav-link" href="?action=tableauBord">Tableau de bord</a>
                            </li>
                            <li class="nav-item <?php if ($_SESSION['menu'] == "comptes") {echo "active";} ?>">
                                <a class="nav-link" href="?action=AfficherModifierCompte">Modifier mon compte</a>
                            </li>
                    <?php } ?>
                <li class="nav-item pl-5"></li> <!--  pour rajouter un espace.. -->
                </ul>
                <form action="?action=deconnexion" method="POST" class="form-inline my-md-0 ml-2">
                    <button class="btn btn-outline-warning my-2 my-sm-0" type="submit">Déconnexion</button>
                </form>
            <?php } ?>
        </div>
    </div>
</nav>

<!-- Pour garder le tab actif (ne fonctionne pas)-->
<script>
$(document).ready(function(){
    $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });
    var activeTab = localStorage.getItem('activeTab');
    if(activeTab){
        $('#myTab a[href="' + activeTab + '"]').tab('show');
    }
});
</script>