<div class="container">
    <h1 class="titre">Rédaction d'un message</h1>
    <?php
    require_once('./modele/classes/Compte.class.php');
    require_once('./modele/dao/CompteDAO.class.php');
        
    if (isset($_REQUEST["message_erreur"])) {
        echo "<div class='alert alert-danger'>";
        echo $_REQUEST["message_erreur"];
        echo "</div>";
    } elseif (isset($_REQUEST["message_succes"])) {
        echo "<div class='alert alert-success'>";
        echo $_REQUEST["message_succes"];
        echo "</div>";
    }
    
    $dao = new CompteDAO();
    $listeEtuds = $dao->findAllRelationCompte($_SESSION["connecte"]["id"],1);
    if ($listeEtuds == false) { ?>
        <h2>Vous n'avez aucun étudiant. Veillez créer des comptes étudiants <button class='btn btn-info' onclick="window.location.href = '?action=formAjoutCompte';"><strong>ICI</strong></button></h2>
    <?php } else { ?>
    <form action="" method="post">
        <div class="form-group">
            <div class="col-sm-8 col-md-6 col-lg-4 col-xl-4" style="padding: 0">
                <label for="titre">Titre :</label>
                <input class="form-control" maxlength="50" name="titre" type="text" required/>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <label for="destinataire">Destinataire :</label>
                <select class="form-control" name='destinataire'>
                    <?php
                    while ($listeEtuds->next()) {
                        $p = $listeEtuds->getCurrent();
                        if ($p != null) {
                            echo "<option value='" . $p->getId() . "'>" . $p->getNom() . "</option>";
                        }
                    }
                    ?>
                </select> 
            </div>
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-4">
                <label for="categorie">Type de message :</label>
                <select class="form-control" name='categorie'>
                    <option value=2>Normal</option>
                    <option value=0>Important</option>
                </select> 
            </div>
        </div>

        
        
        <div class="form-group">
            <div class="col-xl-8" style="padding: 0">
                <label for="contenu">Message :</label><br/>
                <textarea class="form-control" maxlength="255" rows="5" name="contenu" required></textarea>
            </div>
        </div>

        <input name="action" value="envoyerMessage" type="hidden" />
        <input type="submit" class="btn btn-success btn-action" value="Envoyer"/>
        <a class="btn btn-danger btn-action" href="?action=accueil">Annuler</a>
    </form>
    <?php } ?>
</div>