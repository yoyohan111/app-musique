<script>
    function ActionTache(tacheId, userId, actionTache) {
        $.ajax({
            type: "POST",
            url: "ajax/TacheEleveAjaxAction.php",
            data: {
                tacheId: tacheId,
                userId: userId,
                actionTache: actionTache
            },
            success: function(data) {
                //$('#' + tacheId).preventDefault();
                $('#' + tacheId).load(window.location.href + " #" + tacheId + " > *");
                console.log(window.location.href + " #" + tacheId + " > *");
            }
        });
    }
    //document.location.reload(true); //pour garder le positionnement de la page après un reload
</script>
<div class="container">
    <?php
    require_once('./modele/classes/Message.class.php');
    require_once('./modele/dao/MessageDAO.class.php');
    require_once('./modele/classes/Tache.class.php');
    include_once('./modele/classes/StatutTacheEleve.class.php');
    require_once('./modele/dao/TacheDAO.class.php');
    require_once('./modele/dao/CompteDAO.class.php');

    //pour la date
    date_default_timezone_set('America/Toronto');  //asigner le fuseau horaire par défaut à ici
    setlocale(LC_TIME, 'fr_FR.utf8', 'fra.utf8');  //pour mettre la date en français!
    $today = strftime("%A %e %B %G");
    echo "<h3>Le " . $today . "</h3>";

    if (isset($_SESSION["date"])) {
        $dt_min = new DateTime($_SESSION["date"]);
    } else {
        $dt_min = new DateTime("last sunday");
    }
    $dt_max = clone ($dt_min);
    $dt_max->modify('+6 days');
    $mdao = new MessageDAO();
    $listeMessages = $mdao->findAllByDestinataireDate($_SESSION["connecte"]["id"], $dt_min->format('Y-m-d'));
    $tdao = new TacheDAO();
    $listeTaches = $tdao->findAllDate($dt_min->format('Y-m-d'));
    $dt_min_changement = clone ($dt_min);
    ?>

    <h1>
        Bonjour <?php echo $_SESSION['connecte']['nom'] ?>
    </h1>
    <h3>
        <?php
        echo 'Agenda pour la semaine du ' . $dt_min->format('d-m-Y') . ' au ' . $dt_max->format('d-m-Y');
        ?>
    </h3>
    <div>
        <div class="p-3">
            <a href="?action=tableauBord&date=<?php $dt_min_changement->modify('-7 days');
                                                echo $dt_min_changement->format('Y-m-d') ?>">
                <img class="noselect" alt="flechegauche" src="./styles/images/left-arrow.png" width="60px">
            </a>

            <?php
            if ($dt_min != new DateTime('last sunday')) {
                echo '<a style="float: right;" href="?action=tableauBord&date=';
                $dt_min_changement->modify("+14 days");
                echo $dt_min_changement->format("Y-m-d");
                echo '"><img class="noselect" alt="flechedroite" src="./styles/images/right-arrow.png" width="60px"></a>';
            };
            ?>
        </div>
    </div>
    <div class="col-xs conteneur-generique" style="background-color: lightgray">
        <h3 class="titre-conteneur-etu noselect">Tâches</h3>

        <?php

        $now = time();

        if ($listeTaches != false) {
            foreach ($listeTaches as $t) { //$t = Obj Tache
                var_dump($t);
                $statut_tache = TacheDAO::get_statut_tache($t->getIdTache(), $_SESSION["connecte"]["id"]);
                //Formatter la date de création de la tâche avant de l'afficher
                $date_sql = strtotime($t->getDateCreation());
                $date_tache = date('d M', $date_sql);
                $nb_jour = $now - $date_sql;
                ?>
                <div id="<?php echo $t->getIdTache(); ?>" class="card w-100 mb-3 shadow bg-white rounded">
                    <div class="card-body pb-1 pt-3">
                        <h5 class="card-title"><strong><?= $t->getNom() ?></strong></h5>
                        <?php
                        if ($t->getDescription() != "") { //s'il y a une description à inclure: 
                            ?>
                            <p class="card-text mb-2"><?= $t->getDescription() ?></p>
                            <?php }
                        if ($t->getNomFichier() != "") { //s'il y a un fichier à inclure: 
                            if ($t->getTypeFichier() == "audio/mp3" || $t->getTypeFichier() == "audio/mpeg") { //si c'est un fichier audio mp3 ou mpeg
                                ?>
                                <audio controls class='col-12 p-0'>
                                    <source src="<?= $t->getPathFichier() ?>" type="audio/mp3">
                                    Votre navigateur web ne supporte pas l'élément audio.
                                </audio>
                            <?php } else { ?>
                                <p class="card-text mb-2"><i>Fichier inclu :</u></i> <a target='_blank' href='<?= $t->getPathFichier() ?>'><u><?= $t->getNomFichier() ?></u></a></p>
                        <?php }
                        } ?>
                        <p class="card-text"><small class="text-muted noselect">donné le <?= $date_tache ?> (il y a <?php echo round($nb_jour / (60 * 60 * 24)); ?> jours) par <?= $t->getIdProf() ? (CompteDAO::find($t->getIdProf()))->getNom() : "<Professeur inconnu>" ?></small></p>
                    </div>
                    <div class="card-footer bg-transparent inline pb-1 pt-1" style="display: inline-block;">
                        <table>
                            <col width="100%">
                            <col width="0%">
                            <tr>
                                <?php if ($statut_tache->getEstTermine() == 0) { ?>
                                    <th><a class="link btn-terminer" style="color: #007bff; cursor:pointer;" onClick="ActionTache('<?php echo $t->getIdTache(); ?>', '<?php echo $_SESSION["connecte"]["id"] ?>', 'Terminer');"><b class="terminer-text noselect">Compléter</b><i class="far fa-check-square fa-2x ml-1"></i></a></th>
                                <?php } else { //si la tâche est terminé: 
                                ?>
                                    <th><img class="noselect" width="44px" src="styles/images/check_green.png"><span class="noselect" style="vertical-align: sub; color: green;">Fini</span><a class="link" style="vertical-align: sub; cursor:pointer;" onClick="ActionTache('<?php echo $t->getIdTache(); ?>', '<?php echo $_SESSION["connecte"]["id"] ?>', 'Redo')"><i class="fas fa-redo p-2"></i></a></th>
                                <?php }
                                if ($t->getInclureIteration() == 1) { //si il y a une ittération pour la tâche: 
                                ?>
                                    <th><i style="cursor:pointer;" class="fas fa-minus-circle fa-2x" onClick="ActionTache('<?php echo $t->getIdTache(); ?>', '<?php echo $_SESSION["connecte"]["id"] ?>', 'MinusItt');"></i></th>
                                    <th>
                                        <h5><b style="vertical-align: sub;" class="noselect ml-1 mr-1"><?= $statut_tache->getNbIteration() ?></b></h5>
                                    </th>
                                    <th><i style="cursor:pointer;" class="fas fa-plus-circle fa-2x" onClick="ActionTache('<?php echo $t->getIdTache(); ?>', '<?php echo $_SESSION["connecte"]["id"] ?>', 'PlusItt');"></i></th>
                                <?php } ?>
                            </tr>
                        </table>
                    </div>
                </div>
        <?php
            }
        } else {
            echo "<h5>Aucune tâche</h5>";
        }
        ?>

    </div>
    <div class="col-xs conteneur-generique">
        <h3 class="titre-conteneur-etu">Messages</h3>
        <table class="table-etu">
            <tr class="tr-etu">
                <th class="th-etu">Date envoi</th>
                <th class="th-etu">Message</th>
            </tr>
            <?php
            while ($listeMessages->next()) {
                $m = $listeMessages->getCurrent();
                if ($m != null) {
                    echo "<tr class='tr-etu'><td class='td-etu'>" . $m->getDate_envoi() . "</td><td class='td-etu'>" . $m->getContenu() . "</td></tr>";
                }
            }
            ?>
        </table>
    </div>
</div>