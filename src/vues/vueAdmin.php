<?php
    require_once('./modele/dao/CompteDAO.class.php');
    $dao = new CompteDAO();
?>

<div class="container">
    <div class="row">
        <div class="col-sm center-block text-center">
        <h1>Il y a présentement <?php echo $dao::getNbComptes() ?> comptes au total.</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm center-block text-center m-3">
            <a href="?action=formAjoutCompte" class="btn btn-primary btn-tableau-bord p-3">Créer un compte Professeur</a>
        </div>
        <div class="col-sm center-block text-center m-3">
            <a href="?action=listeUtilisateurs" class="btn btn-primary btn-tableau-bord p-3">Voir la liste des utilisateurs</a>
        </div>
    </div>
</div>
