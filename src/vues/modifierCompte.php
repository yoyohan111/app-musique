<div class="container">
<h1>Modifier mon compte</h1>
<?php
    // Affichage de messages d'erreurs
    if (ISSET($_REQUEST["message_erreur"])) {
        echo "<div class='alert alert-danger'>";
        echo $_REQUEST["message_erreur"];
        echo "</div>";
    }
    
    // Affichage de message de confirmation
    if (ISSET($_REQUEST["message_confirmation"])) {
        echo "<div class='alert alert-success' role='alert'>";
        echo $_REQUEST["message_confirmation"];
        echo "</div>";
    }
    
    $ancCourr = "";
    $modCourr = "";
    $conCourr = "";
    if (ISSET($_REQUEST["AncCourriel"])){
        $ancCourr = $_REQUEST["AncCourriel"];
    }
    if (ISSET($_REQUEST["ModCourriel"])){
        $modCourr = $_REQUEST["ModCourriel"];
    }
    if (ISSET($_REQUEST["ConCourriel"])){
        $conCourr = $_REQUEST["ConCourriel"];
    }
?>
<div class="row">
<div class="col-md-6">
    <form action="" method="POST" class="mb-5">
    <h4><i><b>Changer mon adresse courriel :</b></i></h4>
    <br>
        <div class="form-group">
            <div class="col-lg-8" style="padding: 0">
                <label for="AncCourriel">Ancien courriel:</label>
                <input class="form-control" name="AncCourriel" type="text" value="<?php echo $ancCourr?>" autocomplete="new-password" required/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-8" style="padding: 0">
                <label for="ModCourriel">Nouveau courriel:</label>
                <input class="form-control" name="ModCourriel" type="text" value="<?php echo $modCourr?>" autocomplete="new-password" required/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-8" style="padding: 0">
                <label for="ConCourriel">Confirmation nouveau courriel:</label>
                <input class="form-control" name="ConCourriel" type="text" value="<?php echo $conCourr?>" autocomplete="new-password" required/>
            </div>
        </div>
        <input name="action" value="modifierCompte" type="hidden"/>
        <input class="btn btn-primary" type="submit" name="ModifierCourriel" value="Modifier Courriel"/>
    </form>
</div>
</div>

