<div class="container">
    <?php
    require_once('./modele/classes/Tache.class.php');
    require_once('./modele/dao/TacheDAO.class.php');
    require_once('./modele/classes/StatutTacheEleve.class.php');

    //pour la date
    date_default_timezone_set('America/Toronto');  //asigner le fuseau horaire par défaut à ici
    setlocale(LC_ALL, 'fr_FR.utf8', 'fra.utf8'); //pour mettre la date en français!
    $today = strftime("%A %e %B %G");
    echo "<h3>Le " . $today . "</h3>";

    if (isset($_SESSION["connecte"])) { //si un compte est connecté..
    ?>
        <div class='list-group'>
            <?php
            $liste_taches = TacheDAO::FindAllForConnectedUser(); //retourne false si le requête sql n'a pas fonctionné

            if ($liste_taches != false) {
                if ($_SESSION["connecte"]["role"] == 0) {  //si c'est un Admin
                    echo "<h1>Tout les tâches de la base de donnée</h1>";
                } elseif ($_SESSION["connecte"]["role"] == 1) {  //si c'est un Proff
                    echo "<h1>Tâches donnés:</h1>";
                } elseif ($_SESSION["connecte"]["role"] == 2) {  //si c'est un Élève
                    echo "<h1>Tout mes travaux:</h1>";
                }

                $now = time();
            ?>
                <div id="taches">
                    <?php
                    foreach ($liste_taches as $t) { //$t = un objet Tache
                        $d_creation = strtotime($t->getDateCreation());
                        $nb_jour = $now - $d_creation;
                        if ($t != null) { //affichage des tâches 
                    ?>
                            <div class="divtache mb-3">
                                <div class="form-group">
                                    <h5 class="d-inline-block noselect"><b><?= $t->getNom() ?></b></h5>
                                    <div class="d-inline-block float-right">
                                        <button id="btnSupprTache" type="button" class="btn btn-danger mb-3" onclick="location.href='?action=SupprimerTacheAction&id=<?= $t->getIdTache() ?>'">Supprimer</button>
                                    </div>
                                </div>

                                <?php //description
                                if ($t->getDescription() != "") { ?>
                                    <div class="form-group">
                                        <label><?= $t->getDescription() ?></label>
                                    </div>
                                <?php }
                                //Fichier
                                if ($t->getPathFichier() != "") {   //s'il y a un fichier inclu à la tâche...
                                ?>
                                    <div class="form-group">
                                        <label><b>Fichier inclu: </b></label>
                                        <b><a target='_blank' href='<?= $t->getPathFichier() ?>'><?= $t->getNomFichier() ?></a></b>
                                    </div>
                                <?php
                                } ?>

                                <div class="form-group">
                                    <label><b><u>Assigné à : </u></b>
                                        <?php
                                        $liste_info_tache = TacheDAO::get_eleves_tache($t);
                                        foreach ($liste_info_tache as $info_tache) {
                                        ?>
                                            <div>
                                                <span> <?php echo $info_tache[0]; ?> </span>
                                                <span> (<?php echo $info_tache[1]; ?>) : </span>
                                                <?php if ($info_tache[2] == 1) {
                                                    echo "<span style='color:green;'>Terminé</span>";
                                                } else {
                                                    echo "<span style='color:red;'>Non terminé</span>";
                                                }
                                                if ($t->getInclureIteration() == 1) {   //si la taâche inclue une itération
                                                    if ($info_tache[3] != null) {
                                                        if ($info_tache[3] == 0) {
                                                            echo "<span>, <span style='color:cyan;'>Aucune</span> itération effectuée.</span>";
                                                        } elseif ($info_tache[3] == 1) {   //pour le pluriel ou pas
                                                            echo "<span>, <span style='color:cyan;'>" . $info_tache[3] . "</span> itération effectuée.</span>";
                                                        } elseif ($info_tache[3] >= 2) {
                                                            echo "<span>, <span style='color:cyan;'>" . $info_tache[3] . "</span> itérations effectuées.</span>";
                                                        }
                                                    }
                                                } ?>
                                            </div>
                                        <?php }
                                        ?>
                                </div>

                                <?php
                                //Formatter la date de création de la tâche avant de l'afficher
                                $date_sql = strtotime($t->getDateCreation());
                                $date_tache = date('d M', $date_sql);
                                ?>

                                <div class="form-group mb-0">
                                    <label><b>Date de créations: </b><?= $date_tache ?></label>
                                    <medium>, il y a <?php echo round($nb_jour / (60 * 60 * 24)); ?> jours</medium>
                                </div>

                            </div>
            <?php
                        }
                    }
                    echo "</div>";  //pour fermer le div du list-group
                } else {
                    echo "<h2>Aucune tâche n'a été envoyé.</h2>";
                }
            } else {
                echo "<h3><b>Vous devez d'habord vous connecter à partir de la <a href=?action=afficherConnexion>page de connection</a>!</b></h3>";
            }
            ?>
                </div>