<!DOCTYPE html>
<html lang="fr">
<!--Ouvre le html ici et le ferme dans le footer -->

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>École de musique</title>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

  <!-- Local link for redundancy in case public CDN server is down. -->
  <link rel="stylesheet" type="text/css" href="styles/css/bootstrap.min.css" />

  <!-- Custom CSS -->
  <link rel="stylesheet" href="./styles/css/style.css" />

  <!-- Google Font (doit loader après qui le contenue de la page s'est affiché pour que ça affiche correctement) -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700&display=swap" rel="stylesheet" />

  <!-- FontAwesome -->
  <script src="https://kit.fontawesome.com/2c525fd3cf.js" crossorigin="anonymous"></script>

  <!-- Requirement jQuery -->
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <!--
  <script
			  src="https://code.jquery.com/jquery-3.4.1.js"
			  integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
        -->


  <!-- Datatable Sorting and search! 
  <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script> -->
</head>

<body>
  <!-- ouvrir le body et le fermer dans le footer-->