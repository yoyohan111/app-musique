<?php
if (isset($_SESSION["connecte"]["role"])){
    if ($_SESSION["connecte"]["role"] == 0) {
        $type_ajout_compte = 1; //l'Admin ajoute des profs
        $nom_type_ajout_compte = "Professeur";
    } elseif ($_SESSION["connecte"]["role"] == 1) {
        $type_ajout_compte = 2; //Le prof ajoute des élèves
        $nom_type_ajout_compte = "Élève";
    }
}

?>
<div class="container">
    <h1 class="titre">Ajouter un <?php echo $nom_type_ajout_compte; ?></h1>
    <?php
    if (isset($_REQUEST["message_erreur"])) {
        echo "<div class='alert alert-danger'>";
        echo $_REQUEST["message_erreur"];
        echo "</div>";
    }
    ?>

    <form action="" method="post">
        <div class="form-group">
            <div class="col-sm-8 col-md-6 col-lg-4 col-xl-4" style="padding: 0">
                <label for="nomComplet">Nom complet</label><br />
                <input class="form-control" name="nomComplet" type="text" autocomplete="new-password" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-md-6 col-lg-4 col-xl-4" style="padding: 0">
                <label for="courriel">Adresse courriel:</label><br />
                <input class="form-control" name="courriel" type="email" autocomplete="new-password" required />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-8 col-md-6 col-lg-4 col-xl-4" style="padding: 0">
                <label for="confCourriel">Confirmation de l'adresse courriel:</label><br />
                <input class="form-control" name="confCourriel" type="email" autocomplete="new-password" required />
            </div>
        </div>

        <input name="action" value="ajouterCompte" type="hidden" />
        <input type="reset" class="btn btn-danger mr-2" />

        <input type="hidden" value="<?= $type_ajout_compte ?>" name="role" /> <!-- pour mettre le role pour le compteà ajouter -->
        <input type="submit" class="btn btn-success" value="Ajouter le compte" />

    </form>
</div>