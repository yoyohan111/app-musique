<head>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>

<div class="container">
    <form action="?action=AjoutTravail" method="POST" id="form_nouveau_travail" enctype="multipart/form-data"> <!-- onsubmit="OnFormSubmit()" --> 
        <?php
        date_default_timezone_set('America/Toronto');   //asigner le fuseau horaire par défaut à ici
        setlocale(LC_ALL, 'fr_FR.utf8', 'fra.utf8'); //pour mettre la date en français!
        $today = strftime("%A %e %B %G");
        echo "<h3>Le " . $today . "</h3>";
        ?>
        <h3>&nbsp;</h3>
        <!--        Tâches -->
        <div id="taches">
        <div class="divtache pb-0 pt-2" id="divtache1">
            <div class="form-group">
                <h3 class="d-inline-block noselect"><b id="num_tache1">Tâche #1:</b></h3>
                <div class="d-inline">
                    <button id="btnSuppr1" type="button" class="btn btn-danger float-right"  onclick="removeTache(1)">Supprimer</button>
                </div>
                <div class="col p-0">
                    <input id="nom_tache1" class="form-control mt-1 d-inline" type="text" name="TacheNom1" placeholder="* Titre" required>  </span>
                </div>
            </div>

            <!--        Description -->
            <div class="form-group">
                <label class="noselect"><b>Description de la tâche (si nécessaire):</b></label><br>
                <textarea id="Textarea_description1" class="form-control" name="TacheDescription1" rows="3" placeholder="Entrez les explications de la tâche ici..."></textarea>
            </div>
            <!--        Ajout d'un Fichier à la tâche -->
            <div class="form-group mb-0">
                <label class="noselect"><b>Inclure un fichier...</b></label>
                <div class="custom-file file-input" style="display: flex">
                    <input type="file" class="custom-file-input" name="Fichier1" id="fichier1" accept="image/png, image/jpeg, audio/mpeg, .pdf, .doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" style="cursor: pointer; text-indent: -999px;"/> <!-- ici le text-indent pour que le cursor: pointer fonctionne correctement -->
                    <label class="custom-file-label" for="customFile" data-browse="Parcourir"><i class="fas fa-file-upload fa-lg"></i>&nbsp Aucun fichier</label>
                </div>
            </div>
            <!--        nb iteration -->
            <div class="custom-control form-control-lg custom-checkbox text-center">
                <input class="custom-control-input" type="checkbox" name="InclureIteration[]" id="InclureIteration1" value="1">
                <label class="custom-control-label noselect itt-check" id="LabelItt1" for="InclureIteration1">&nbspInclure les itérations</label>
            </div>
        </div>
        <p class="mb-0" id="espace1">&nbsp;</p>
        </div>

        <input name="nb_taches" value="1" type="hidden" />	<!-- Pour savoir combien de tâches il y a.. -->

        <!--        Bouton pour l'ajout d'une nouvelle tâche -->
        <button type="button" class="btn btn-info" onclick="addTache()">
            <span class="fas fa-plus fa"></span>  Nouvelle tâche
        </button>

        <h3>&nbsp;</h3>
        <!--        Sélection des élèves concernés des tâches donnés -->
        <div class="form-group">
            <label class="noselect"><b>Assigner Tâches à:</b></label>
            <div class="accordion" id="accordionExample">
                <div class="card">
                <span id="ErrorMessageEleve" class="warningMessage" style='color: red;'>Veuillez cocher au moins un élève</span>
                    <div class="card-header" id="heading1">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">ÉLÈVES <img name="req" src="./styles/images/required_icon.svg" style="vertical-align: top;"/>
                            </button>
                        </h5>
                    </div>
                    
                    <div id="collapse1" class="collapse" aria-labelledby="heading1" data-parent="#accordionExample">
                        <div class="card-body">
                            <div class="form-group">
                                
                                <?php
                                include_once('./modele/dao/CompteDAO.class.php');
                                $liste_eleves = CompteDAO::findAllRelationCompte($_SESSION["connecte"]["id"],1);   //role de 2 pour un eleve

                                if ($liste_eleves != false) {   //est false si vide
                                    while ($liste_eleves ->next()) {
                                        $p = $liste_eleves->getCurrent();
                                        if ($p != null) {
                                            ?>
                                            <div class="custom-control custom-checkbox">
                                                <input class="custom-control-input" type="checkbox" name="TacheIdEleves[]" id="<?= $p->getId() ?>" value="<?= $p->getId() ?>" required="required">
                                                <label class="custom-control-label" for="<?= $p->getId() ?>"><?= $p->getNom() ?></label>
                                            </div>
                                            <?php
                                        }
                                    }
                                } else {
                                    ?>
                                    <div class="form-check">
                                        <p>Vous n'avez aucun étudiant. Veillez créer des comptes étudiants <button class='btn btn-info' onclick="window.location.href = '?action=formAjoutCompte';"><strong>ICI</strong></button></p>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="submit" class="btn btn-primary" name="AjoutTravailBouton" value="Envoyer" />
    </form>
</div>

<script>
    var countNbTache = 1;
    function addTache() {
        countNbTache += 1;
        
        var lastT = document.getElementById("taches");

        lastT.insertAdjacentHTML('beforeend',
        '<div class="divtache pb-0 pt-2" id="divtache' + countNbTache + '">' +
            '<div class="form-group">' +
                '<h3 class="d-inline-block noselect"><b id="num_tache' + countNbTache + '">Tâche #' + countNbTache + ':</b></h3>' +
                '<div class="d-inline">' +
                    '<button id="btnSuppr' + countNbTache + '" type="button" class="btn btn-danger float-right" onclick="removeTache(' + countNbTache + ')">Supprimer</button>' +
                '</div>' +
                '<div class="col p-0">' +
                    '<input id="nom_tache' + countNbTache + '" class="form-control mt-1" type="text" name="TacheNom' + countNbTache + '" placeholder="* Titre" required>' +
                '</div>' +
            '</div>' +
            '<div class="form-group">' +
                    '<label class="noselect"><b>Description de la tâche (si nécessaire):</b></label><br>' +
                    '<textarea id="Textarea_description' + countNbTache + '" class="form-control" name="TacheDescription' + countNbTache + '" rows="3" placeholder="Entrez les explications de la tâche ici..."></textarea>' +
            '</div>' +
            '<div class="form-group mb-0">' +
                '<label class="noselect"><b>Inclure un fichier...</b></label>' +
                '<div class="custom-file file-input" style="display: flex">' +
                    '<input type="file" class="custom-file-input" name="Fichier' + countNbTache + '" id="fichier' + countNbTache + '" style="cursor: pointer; text-indent: -999px;"/>' +
                    '<label class="custom-file-label" for="customFile" data-browse="Parcourir"><i class="fas fa-file-upload fa-lg"></i>&nbsp Aucun fichier</label>' +
                '</div>' +
            '</div>' +
            '<div class="custom-control form-control-lg custom-checkbox text-center">' +
                '<input class="custom-control-input" type="checkbox" name="InclureIteration[]" id="InclureIteration' + countNbTache + '" value="' + countNbTache + '">' +
                '<label class="custom-control-label noselect itt-check" id="LabelItt' + countNbTache + '" for="InclureIteration' + countNbTache + '">&nbspInclure les itérations</label>' +
            '</div>' +
        '</div>' +
        '<p class="mb-0" id="espace' + countNbTache + '">&nbsp;</p>'
        );

        $('input[name=nb_taches]').val(countNbTache);	//pour que l'application sache combien il y a de tâches..

        
    }
    function removeTache(x) {   //pas mal de la SAUCE épaisse ça... xd
        $("#divtache" + x).remove();
        $("#espace" + x).remove();

        for (i = x + 1; i <= countNbTache; i++) {	//Décaler toutes les numero de tâches après celle supprimé par 1.
            var y = i - 1;	//pour la décrémentation
            //div
            $('#divtache' + i).prop('id', 'divtache' + y);
            //h3 id et contenue (changer le contenue avant de changer le id car ça prend le id présent pour changer le contenue.....)
            document.getElementById('num_tache' + i).innerHTML = 'Tâche #' + y + ':';
            $('#num_tache' + i).prop('id', 'num_tache' + y);
            //bouton supprimer (modifier la fonction en premier! puisque ça le prend par le id...)
            $('#btnSuppr' + i).attr('onclick', 'removeTache(' + y + ')');
            $('#btnSuppr' + i).prop('id', 'btnSuppr' + y);
            //nomTache
            $('#nom_tache' + i).prop('id', 'nom_tache' + y);
            $('input[name=TacheNom' + i + ']').prop('name', 'TacheNom' + y);
            //description
            $('#Textarea_description' + i).prop('id', 'Textarea_description' + y);
            $('textarea[name=TacheDescription' + i + ']').prop('name', 'TacheDescription' + y);
            //fichier
            $('input[name=Fichier' + i + ']').prop('name', 'Fichier' + y);
            $('#fichier' + i).prop('id', 'fichier' + y);
            //nb Itération
            $('#InclureIteration' + i).prop('value', y);
            $('#InclureIteration' + i).prop('id', 'InclureIteration' + y);
            $('#LabelItt' + i).attr('for', 'InclureIteration' + y);
            $('#LabelItt' + i).prop('id', 'LabelItt' + y);
            //espace..
            $('#espace' + i).prop('id', 'espace' + y);
        }
        countNbTache--;
        $('input[name=nb_taches]').val(countNbTache);	//pour que l'application sache combien il y a de tâches..
    }

    $(document).on('input', '.custom-file-input', function(e){  //pour afficher le nom du fichier après le téléchargement
        $(this).next('.custom-file-label').text(e.target.files[0].name);
    });

    jQuery(function ($) { //pour s'assurer qu'au moins un checkbox est sélectionné pour la liste des élèves
        var requiredCheckboxes = $(':checkbox[required]');
        requiredCheckboxes.on('change', function (e) {
            console.log("allo test22");
            var checkboxGroup = requiredCheckboxes.filter('[name="' + $(this).attr('name') + '"]');
            var isChecked = checkboxGroup.is(':checked');
            checkboxGroup.prop('required', !isChecked);
        });
        requiredCheckboxes.trigger('change');
    });

    $('span#ErrorMessageEleve').hide();

    $("input[type=file]").change(function (e){$(this).next('.custom-file-label').text(e.target.files[0].name);})    //pour le nom du fichier


</script>
