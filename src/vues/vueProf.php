<header>
    <h1 style="text-align: center" id="Titre">Bonjour <?php echo $_SESSION['connecte']['nom']; ?></h1>
</header>

<div class="container justify-content-center col-12" style="text-align: center;">
    <div class="type-1 mb-3">
        <a href="?action=afficherListeTaches" class="btn btn-1 btn-orange">
            <span class="btxt">Consulter les tâches données</span>
            <span class="round"><i class="fa fa-chevron-right fa-lg"></i></span>
        </a>
    </div>
    <div class="type-1 mb-3">
        <a href="?action=AfficherAjoutTravail" class="btn btn-1 btn-blue">
            <span class="btxt">Ajouter une nouvelle tâche</span>
            <span class="round"><i class="fa fa-plus fa-lg"></i></span>
        </a>
    </div>
    <div class="type-1 mb-3">
        <a href="?action=formRedactionMessageProf" class="btn btn-1 btn-black">
            <span class="btxt">Rédiger un mémo</span>
            <span class="round"><i class="far fa-envelope fa-lg"></i></span>
        </a>
    </div>
</div>