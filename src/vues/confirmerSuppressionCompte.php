<?php
    require_once('./modele/classes/Compte.class.php');
    require_once('./modele/dao/CompteDAO.class.php');

    $dao = new CompteDAO();
    $c = $dao->find($_REQUEST["idASupprimer"]);
    $role = "";
    switch ($c->getRole()) {
        case 1 :
            $role = "Professeur";
            break;
        case 2 :
            $role = "Étudiant";
            break;
        default:
            $role = "Non reconnu (brisé?)";
    }
?>
<div class="container">
    <h1 id="titre-confirmation">Voulez-vous vraiment supprimer ce compte?</h1>
    <div class="conteneur-confirmation">
    <?php 
        echo "<p>".$c->getNom()." (".$c->getCourriel()."), ".$role."</p>";
        echo '<a href="?action=listeUtilisateurs" class="btn btn-action btn-danger">Non</a><a class="btn btn-action btn-success" href="?action=supprimerCompte&idASupprimer='.$_REQUEST["idASupprimer"].'">Oui</a>';
    ?>    
</div>


