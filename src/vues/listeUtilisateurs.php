<!-- Pour le DataTable -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.20/r-2.2.3/sc-2.0.1/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.20/r-2.2.3/sc-2.0.1/datatables.min.js"></script>
 
<?php
    require_once('./modele/classes/Compte.class.php');
    require_once('./modele/dao/CompteDAO.class.php');

    $dao = new CompteDAO();
?>

<div class="container">
    <?php
        if (isset($_REQUEST["message_erreur"])) {
            echo "<div class='alert alert-danger'>";
            echo $_REQUEST["message_erreur"];
            echo "</div>";
        } elseif (isset($_REQUEST["message_succes"])) {
            echo "<div class='alert alert-success'>";
            echo $_REQUEST["message_succes"];
            echo "</div>";
        }

    $listeEtuds = $dao->findAllByRole(2);
    if ($listeEtuds != false) {
        echo "<h2>" . $listeEtuds->size() . " étudiants d'inscrits:</h2>";
        ?>
        <table class="table table-bordered mydatatable" style="width: 100%">
            <thead>
                <tr><th>Identifiant</th><th>Nom complet</th><th>Adresse courriel</th><th>Actions</th></tr>
            </thead>
            <tbody class="body-datatable">
                <?php 
                while ($listeEtuds->next()) {
                    $p = $listeEtuds->getCurrent();
                    if ($p != null) {
                        echo "<tr><td>".$p->getId()."</td><td>".$p->getNom()."</td><td>".$p->getCourriel()."</td><td style='padding: 5px 12px;'><a class='btn btn-danger' href='?action=confirmerSuppressionCompte&idASupprimer=" . $p->getId() . "'>Supprimer</a></td></tr>";
                    }
                }
                ?>
            </tbody>
        </table>
        <br>   
    <?php } else {
        echo '<h2>Aucuns étudiants inscrits</h2>';
    } 
    
    $listeProfs = $dao->findAllByRole(1);
    if ($listeProfs != false) { 
        echo "<h2>" . $listeProfs->size() . " professeurs d'inscrits:</h2>";
        ?>
        <table class="table table-bordered mydatatable" style="width: 100%;">
            <thead>
                <tr><th>Identifiant</th><th>Nom complet</th><th>Adresse courriel</th><th>Actions</th></tr>
            </thead>
            <tbody class="body-datatable">
            <?php
            while ($listeProfs->next()) {
                $p = $listeProfs->getCurrent();
                if ($p != null) {
                    echo "<tr><td>".$p->getId()."</td><td>".$p->getNom()."</td><td>".$p->getCourriel()."</td><td style='padding: 5px 12px;'><a class='btn btn-danger' href='?action=confirmerSuppressionCompte&idASupprimer=" . $p->getId() . "'>Supprimer</a></td></tr>";
                }
            }
            ?>
            </tbody>
        </table>
    <?php } else {
        echo '<h2>Aucuns professeurs inscrits</h2>';
    } ?>
</div>

<script>
    $('.mydatatable').DataTable({
        lengthMenu: [[5,25,-1], [5,25,"All"]],
        scrollY: 400,
        scrollX: true,
        scrollCollapse: true,
        "language": {
            "lengthMenu": "Afficher _MENU_ données par page",
            "zeroRecords": "Aucun compte trouvé",
            "info": "Page _PAGE_ sur _PAGES_",
            "infoEmpty": "Aucunes données disponibles",
            "infoFiltered": "(filtré à partir de _MAX_ enregistrements totaux)",
            "search":         "Recherche:",
            "thousands":      " ",
            "processing":     "En traitement...",
            "decimal":        ",",
            "paginate": {
                "first":      "Premier",
                "last":       "Dernier",
                "next":       "Prochain",
                "previous":   "Précédent"
            }
        }
    });
    //pour les paramêtres de language: https://datatables.net/reference/option/language
</script>