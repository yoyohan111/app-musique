<?php
//pour loader la variable de session pour chaques pages de l'application (première instruction a faire dans index.php)
if (!isset($_SESSION)) {
    session_start();
}

require_once('./controleur/ActionBuilder.class.php');
require_once('./controleur/RequirePRGAction.interface.php');
$action = null;
$vue = null;


if (isset($_REQUEST["action"])) {
    $vue = ActionBuilder::getAction($_REQUEST["action"])->execute();
} else {
    $action = ActionBuilder::getAction("");
    $vue = $action->execute();
}

include_once('./vues/head_html.php'); // Contenu du <head>
include_once('./vues/navbar.php'); // Barre de navigation

if ($action instanceof RequirePRGAction) {
    header("Location: ?action=".$vue);
} else {
    include_once('./vues/'.$vue.'.php');
}

include_once('./vues/footer.php'); // Footer