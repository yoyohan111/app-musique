<?php
//dirname(__FILE__) .
//$_SERVER["DOCUMENT_ROOT"] .
require_once($_SERVER["DOCUMENT_ROOT"]. '/app-musique/src/modele/dao/TacheDAO.class.php');
include_once($_SERVER["DOCUMENT_ROOT"]. '/app-musique/src/modele/classes/Tache.class.php');
include_once($_SERVER["DOCUMENT_ROOT"]. '/app-musique/src/modele/classes/StatutTacheEleve.class.php');

if (isset($_REQUEST['actionTache'])) {
    $statut_tache = TacheDAO::get_statut_tache($_REQUEST['tacheId'], $_REQUEST['userId']);
    if ($_REQUEST['actionTache'] == 'Terminer') {
        $statut_tache->setEstTermine(1);
    } elseif ($_REQUEST['actionTache'] == 'Redo') {
        $statut_tache->setEstTermine(0);
    } elseif ($_REQUEST['actionTache'] == 'MinusItt') {
        $statut_tache->iterationMoins();
    } elseif ($_REQUEST['actionTache'] == 'PlusItt') {
        $statut_tache->iterationPlus();
    }
    TacheDAO::update_statut_tache($statut_tache);
    unset($_REQUEST['actionTache']);
    echo 'true';
} else {
    echo 'false';
}
