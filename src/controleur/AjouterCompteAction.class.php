<?php

require_once('./controleur/Action.interface.php');
require_once('./controleur/RequirePRGAction.interface.php');
require_once('./modele/dao/CompteDAO.class.php');

class AjouterCompteAction implements Action, RequirePRGAction
{
    public function execute()
    {
        //Checker les erreurs avant de commancer...
        if (!isset($_SESSION["connecte"]) || !isset($_SESSION["connecte"]["id"])) {
            $_REQUEST["message_erreur"] = "Vous devez être connecté pour rédiger un message.";
            return "connexion";
        }
        if (
            !isset($_REQUEST["nomComplet"]) ||
            !isset($_REQUEST["courriel"]) ||
            !isset($_REQUEST["confCourriel"]) ||
            !isset($_REQUEST["role"])
        ) {
            $_REQUEST["message_erreur"] = "Des paramètres sont manquants dans la requête.";
            return "formAjoutCompte";
        }

        if (
            $_REQUEST["nomComplet"] == '' ||
            $_REQUEST["courriel"] == '' ||
            $_REQUEST["confCourriel"] == ''
        ) {
            $_REQUEST["message_erreur"] = "Des champs ont été laissés vides.";
            return "formAjoutCompte";
        }

        if ($_REQUEST["courriel"] != $_REQUEST["confCourriel"]) {
            $_REQUEST["message_erreur"] = "Le courriel et la confirmation du courriel doivent être identiques.";
            return "formAjoutCompte";
        }

        //Aucune erreur détecté, alors on crée le compte et on l'ajoute à la base de donnée

        $dao = new CompteDAO();
        $x = new Compte();
        $x->setNom($_REQUEST["nomComplet"]);
        $x->setCourriel($_REQUEST["courriel"]);
        $x->setRole($_REQUEST["role"]);
        if (!$dao->create($x)) {    //si la création du compte n'a pas fonctionné:
            $_REQUEST["message_erreur"] = "ERREUR: La création du compte n'a pas fonctionné!";
            return "formAjoutCompte";
        } else {
            if ($_SESSION["connecte"]["role"] == 1) {  //si c'est un prof qui a ajouté un compte, faire la relation avec l'élève:
                if (CompteDAO::faire_relation_compte($_SESSION["connecte"]["id"], $x->getId())) {
                    $_REQUEST["info_type"] = "ajout_compte";
                    $_REQUEST["info_titre"] = "Compte ajouté!";
                    return "info";
                } else {
                    $_REQUEST["message_erreur"] = "ERREUR: La relation entre l'élève et le professeur n'a pas bien été effectué!";
                    return "formAjoutCompte";
                }
            } else {
                $_REQUEST["info_type"] = "ajout_compte";
                $_REQUEST["info_titre"] = "Compte ajouté!";
                return "info";
            }
        }
    }
}
