<?php
/**
 * Interface d'action
 *
 * @author Alexandre Dupré, Alex Duong
 */
interface Action {
	public function execute();
}
?>
