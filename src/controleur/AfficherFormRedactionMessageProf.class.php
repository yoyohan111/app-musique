<?php

require_once('./controleur/Action.interface.php');

class AfficherFormRedactionMessageProf implements Action {
    public function execute()
    { 
        if (!isset($_SESSION["connecte"]) || !isset($_SESSION["connecte"]["id"])) {
            $_REQUEST["message_erreur"] = "Vous devez être connecté pour rédiger un message.";
            return "connexion";
        }

        return "formRedactionMessageProf";
    }
}
