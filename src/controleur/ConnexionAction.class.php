<?php

require_once('./controleur/Action.interface.php');
require_once('./modele/dao/CompteDAO.class.php');

class ConnexionAction implements Action
{
    public function execute()
    {
        if (!$this->valide()) {
            return "connexion";
        }
        
        $cdao = new CompteDAO();
        $compte = $cdao->findByCourriel($_REQUEST["courriel"]);

        if ($compte == null) {
            $_REQUEST["field_messages"]["courriel"] = "Utilisateur inexistant";
            return "connexion";
        }

        $_SESSION["connecte"]["id"] = $compte->getId();
        $_SESSION["connecte"]["role"] = $compte->getRole();
        $_SESSION["connecte"]["nom"] = $compte->getNom();

        switch ($compte->getRole()) {
            case 0 :
                return "vueAdmin";
            case 1 :
                return "vueProf";
            case 2 :
                return "vueEtudiant";
        }
    }

    public function valide()
    {
        if ($_REQUEST["courriel"] == "" || !isset($_REQUEST["courriel"])) {
            $_REQUEST["message_erreur"] = "Vous devez entrer votre adresse courriel.";
            return false;
        }
        
        // Si valide après verifications
        return true;
    }
}
