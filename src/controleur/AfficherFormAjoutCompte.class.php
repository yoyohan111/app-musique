<?php

require_once('./controleur/Action.interface.php');

class AfficherFormAjoutCompte implements Action
{
    public function execute()
    {
        if (!isset($_SESSION["connecte"]) || !isset($_SESSION["connecte"]["id"])) {
            $_REQUEST["message_erreur"] = "Vous devez être connecté pour ajouter un compte.";
            return "connexion";
        }
        return "formAjoutCompte";
    }
}
