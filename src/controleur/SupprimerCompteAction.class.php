<?php

require_once('./controleur/Action.interface.php');
require_once('./modele/dao/CompteDAO.class.php');

class SupprimerCompteAction implements Action {

    public function execute() {
        if (!isset($_SESSION["connecte"]) || !isset($_SESSION["connecte"]["id"])) {
            $_REQUEST["message_erreur"] = "Vous devez être connecté afin de supprimer un compte.";
            return "connexion";
        }

        if (!isset($_SESSION["connecte"]["role"]) || $_SESSION["connecte"]["role"] > 0) {
            return "erreurNonAutorise";
        }
        if (ISSET($_REQUEST["idASupprimer"])) {
            $x = CompteDAO::find($_REQUEST["idASupprimer"]);
            if (!CompteDAO::delete($x)) {   //si la requete de delete du compte a échoué
                $_REQUEST["message_erreur"] = "Erreur survenu, compte non supprimé.";
            } else {    //si la requete de delete a bien fonctionné
                $_REQUEST["message_succes"] = "Le compte a été supprimé.";
            }
        }
        
        return "listeUtilisateurs";
    }

}

?>