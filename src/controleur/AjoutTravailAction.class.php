<?php
require_once('./controleur/Action.interface.php');
require_once('./modele/dao/TacheDAO.class.php');
require_once('./modele/classes/Tache.class.php');

class AjoutTravailAction implements Action
{
    public function execute()
    {
        if (isset($_REQUEST['AjoutTravailBouton'])) { //si le bouton envoyer a été pesé sur la page d'envoie d'un travail:
            //prendre les variables html de la vue "tache" et les mettre en variable PHP avec $_POST['nom']

            date_default_timezone_set('America/Toronto');   //asigner le fuseau horaire par défaut
            $tache_date_creation = date('Y-m-d');

            $nb_taches = $_REQUEST['nb_taches'];
            //echo "nombre de tâches: " . $nb_taches;

            if ($_SESSION["connecte"]["role"] != 1) { //Checker si c'est un professeur qui essaie d'ajouter une tâche!
                $_REQUEST["info_type"] = "ajout_tache_erreur";
                $_REQUEST["info_numTache"] = $i;
                $_REQUEST["info_message"] = "Vous n'êtes pas un professeur!! vous ne pouvez pas ajouter de tâches!";
                return "info";
            }

            if (isset($_REQUEST['TacheIdEleves'])) {   //test si la variable html est présente avec ISSET
                try {
                    $ListeIdEleves = $_POST["TacheIdEleves"];
                } catch (Exception $e) {
                    $_REQUEST["info_type"] = "ajout_travail_erreur";
                    $_REQUEST["info_message"] = $e->getMessage();
                    return "info";
                }
            } else {
                $_REQUEST["info_type"] = "ajout_travail_erreur";
                $_REQUEST["info_message"] = "Assurez-vous de cocher au moins un élève avant d'envoyer le travail!";
                return "info";
            }
            
            //pour chaque tâches (commence à 1). $i= le numero de tâches
            for ($i = 1; $i <= $nb_taches; $i++) {
                //Créer l'objet Tache
                $ta = new Tache();
                $ta->setDateCreation($tache_date_creation); //assigner sa date de création
                $ta->setIdProf($_SESSION["connecte"]["id"]);  //assigner le id_prof

                if (isset($_REQUEST['TacheNom' . $i])) { //TacheNom
                    try {
                        $ta->setNom($_POST['TacheNom' . $i]);
                    } catch (Exception $e) {
                        $_REQUEST["info_type"] = "ajout_tache_erreur";
                        $_REQUEST["info_numTache"] = $i;
                        $_REQUEST["info_message"] = $e->getMessage();
                        return "info";
                    }
                }
    
                if (isset($_REQUEST['TacheDescription' . $i])) {   //description
                    try {
                        $ta->setDescription($_POST['TacheDescription' . $i]);
                    } catch (Exception $e) {
                        $_REQUEST["info_type"] = "ajout_tache_erreur";
                        $_REQUEST["info_numTache"] = $i;
                        $_REQUEST["info_message"] = $e->getMessage();
                        return "info";
                    }
                }

                //ini_set('mysql.connect_timeout', 500);
                //ini_set('default_socket_timeout', 500);

                //pour le fichier:
                if ($_FILES['Fichier' . $i]['name'] != "") {  //s'il y a un fichier à inclure...
                    try {
                        $file = $_FILES['Fichier' . $i];

                        $file_name = $file['name'];
                        $file_type = $file['type'];
                        $file_tmp = $file['tmp_name'];
                        $file_size = $file['size'];
                        $file_error = $file['error'];

                        //prendre l'extention du fichier
                        $file_ext = explode('.', $file_name);
                        $file_ext = strtolower(end($file_ext));

                        if  ($file_error === 0){
                            $file_name_new = uniqid('', true) . '.' . $file_ext;
                            $file_path = 'fichiers/' . $file_name_new;

                            if (move_uploaded_file($file_tmp, $file_path)) {    //si l'ajout du fichier a bien fonctionné, il le rajoute à l'objet de la tâche
                                $ta->setNomFichier($file_name);
                                $ta->setTypeFichier($file_type);
                                $ta->setPathFichier($file_path);
                            } else {
                                echo "Le fichier n'a pas pu être enregistré!";
                            }
                        } else {
                            echo "Fichier non valide!";
                        }
                        unset($_FILES['Fichier' . $i]);   //unset pour le prochain fichier..
                    } catch (Exception $e) {
                        $_REQUEST["info_type"] = "ajout_fichier_erreur";
                        $_REQUEST["info_numTache"] = $i;
                        $_REQUEST["info_message"] = $e->getMessage();
                        return "info";
                    }
                }
                
                //Pour l'inclusion de l'ittération pour une tâche ça été coché
                if (isset($_REQUEST['InclureIteration'])) {
                    if (in_array($i, $_REQUEST['InclureIteration'])){
                        $ta->setInclureIteration(1);
                    }
                }

                //enregistrer la tâche dans la base de donnée
                $tadao = new TacheDAO();
                if (!$tadao->create($ta)) { //si il y a eu une erreure dans la requête sql pour l'ajout des tâches dans la base de donnée:
                    //ASSIGNER LES MESSAGES D'ERREURS ET RETOURNER LA PAGE D'INFO (PAGE D'ERREURS).
                    $_REQUEST["info_type"] = "ajout_tache_sql";
                    $_REQUEST["info_numTache"] = $i;
                    $_REQUEST["info_message"] = "Assurez-vous que tout les champs ont été remplis.";
                    return "info";

                } else {  //si la requête sql de la TACHE a bien fonctionné:
                    //Créer la relation entre la TACHE et les utilisateurs qui y sont affectés
                    TacheDAO::faire_relation_eleves($ta->getIdTache(), $ListeIdEleves);
                }
            }
            //redirection à la page d'info
            $_REQUEST["info_type"] = "ajout_travail";
            $_REQUEST["info_titre"] = "Tâche(s) bien envoyé!";
            return "info";

        } else {
            $_REQUEST["info_type"] = "redirection";
            $_REQUEST["info_message"] = "ERREUR: Vous devez entrer vos données à partir de la page: <a href='?action=AfficherAjoutTravail'>Donner un Travail</a>.";
            return "info";
        }
        return "AfficherAjoutTravail";
    }
}
