<?php
require_once('./controleur/Action.interface.php');
require_once('./modele/dao/TacheDAO.class.php');
require_once('./modele/classes/Tache.class.php');

class SupprimerTacheAction implements Action
{
    public function execute()
    {
        $id = isset($_GET['id']) ? $_GET['id'] : "";    //prendre le id de la tâche

        $tdao = new TacheDAO();

        if (TacheDAO::deleteById($id)) { //si la suppression de la tâche avec ses relations a bien fonctionné:
            $_REQUEST["info_type"] = "suppr_tache_succes";
        } else {
            $_REQUEST["info_type"] = "suppr_tache_erreur";
        }

        return "info";
    }
}
?>

