<?php

require_once('./controleur/Action.interface.php');
require_once('./controleur/RequirePRGAction.interface.php');
require_once('./modele/dao/MessageDAO.class.php');

class EnvoyerMessageAction implements Action, RequirePRGAction {
    public function execute() {
        if (!isset($_SESSION["connecte"]) || !isset($_SESSION["connecte"]["id"])) {
            $_REQUEST["message_erreur"] = "Vous devez être connecté pour envoyer un message.";
            return "connexion";
        }
        
        if (!isset($_SESSION["connecte"]["role"]) || $_SESSION["connecte"]["role"] > 1){
            return "erreurNonAutorise";
        }
        
        if (!isset($_REQUEST['titre']) ||
            !isset($_REQUEST['destinataire']) ||
            !isset($_REQUEST['categorie']) ||
            !isset($_REQUEST['contenu']))   
        {
            $_REQUEST["message_erreur"] = "Des paramètres sont manquants dans la requête, veuillez contacter l'administrateur.";
            return "formRedactionMessageProf";
        }
        
        if ($_REQUEST['titre'] == '' ||
            $_REQUEST['destinataire'] == '' ||
            $_REQUEST['categorie'] == '' ||
            $_REQUEST['contenu'] == '') 
        {
            $_REQUEST["message_erreur"] = "Veuillez remplir tous les champs.";
            return "formRedactionMessageProf";
        }
        
        if (strlen($_REQUEST['titre']) > 250 || strlen($_REQUEST['contenu']) > 250) {
            $_REQUEST["message_erreur"] = "Le titre et/ou le message dépasse la limite de 250 caractères.";
            return "formRedactionMessageProf";
        }
        
        $dao = new MessageDAO();
        $m = new Message();
        $m->setTitre($_REQUEST['titre']);
        $m->setId_destinataire($_REQUEST['destinataire']);
        $m->setId_expediteur($_SESSION["connecte"]["id"]);
        $m->setEst_lu(0); // Non lu
        $m->setCategorie($_REQUEST['categorie']);
        $m->setContenu($_REQUEST['contenu']);        
            
        if (!$dao->create($m)) {
            $_REQUEST["message_erreur"] = "Une erreur est survenue lors de l'envoi du message, veuillez réessayer.";
            return "formRedactionMessageProf";
        }    
        
        // Si l'envoi du message a été effectué sans erreurs
        $_REQUEST["message_succes"] = "Message envoyé avec succès.";
        return "formRedactionMessageProf";
    }
}

