<?php

require_once('./controleur/Action.interface.php');

class AfficherTableauBordAction implements Action {
    public function execute() {
        if (!isset($_SESSION["connecte"]) || !isset($_SESSION["connecte"]["id"])) {
            $_REQUEST["message_erreur"] = "Veuillez vous connecter.";
            return "connexion";
        }
        
        switch ($_SESSION["connecte"]["role"]) {
            case 0 :
                return "vueAdmin";
            case 1 :
                return "vueProf";
            case 2 :
                if (isset($_REQUEST["date"]))
                    {$_SESSION["date"] = $_REQUEST["date"];}
                return "vueEtudiant";
        }
        
        // Si un role correspondant n'est pas trouvé, on oblige la connexion
        $_REQUEST["message_erreur"] = "Veuillez vous connecter.";
        return "connexion";
    }
}
