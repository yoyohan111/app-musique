<?php

require_once('./controleur/Action.interface.php');
require_once('./modele/dao/CompteDAO.class.php');

class ConfirmerSuppressionCompteAction implements Action {
    public function execute() {
        if (!isset($_SESSION["connecte"]) || !isset($_SESSION["connecte"]["id"])) {
            $_REQUEST["message_erreur"] = "Vous devez être connecté afin de supprimer un compte.";
            return "connexion";
        }

        if (!isset($_SESSION["connecte"]["role"]) || $_SESSION["connecte"]["role"] > 0) {
            return "erreurNonAutorise";
        }
        
        $cdao = new CompteDAO();
        $compte = $cdao->find($_REQUEST["idASupprimer"]);

        if ($compte == null) {
            $_REQUEST["message_erreur"] = "Le compte que vous tentez de supprimer n'existe pas.";
            return "listeUtilisateurs";
        } else {
            return "confirmerSuppressionCompte";
        }
    }
}
?>