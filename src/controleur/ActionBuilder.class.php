<?php

/**
 * Reçoit l'action donnée par l'utilisateur et retourne la vue correspondante.
 *
 * @author Alexandre Dupré, Alex Duong, Christopher Sarao, Yohan Gagnon-K
 */

require_once('./controleur/AfficherConnexionAction.class.php');
require_once('./controleur/AfficherFormAjoutCompte.class.php');
require_once('./controleur/AfficherFormRedactionMessageProf.class.php');
require_once('./controleur/AfficherListeTaches.class.php');
require_once('./controleur/AfficherListeUtilisateursAction.class.php');
require_once('./controleur/AfficherModifierCompteAction.class.php');
require_once('./controleur/AfficherTableauBordAction.class.php');
require_once('./controleur/AfficherTravailEleve.class.php');
require_once('./controleur/AfficherAjoutTravailAction.class.php');
require_once('./controleur/AjoutTravailAction.class.php');
require_once('./controleur/AjouterCompteAction.class.php');
require_once('./controleur/ConnexionAction.class.php');
require_once('./controleur/ConfirmerSuppressionCompteAction.class.php');
require_once('./controleur/DeconnexionAction.class.php');
require_once('./controleur/DefaultAction.class.php');
require_once('./controleur/EnvoyerMessageAction.class.php');
require_once('./controleur/ModifierCompteAction.class.php');
require_once('./controleur/SupprimerCompteAction.class.php');
require_once('./controleur/SupprimerTacheAction.class.php');
require_once('./controleur/TravailFiniAction.class.php');

class ActionBuilder
{
    public static function getAction($nom)
    {
        switch ($nom) {
            //login - logout - inscription
            case "afficherConnexion":
                $_SESSION['menu'] = "connexion";
                return new AfficherConnexionAction();
                break;
            case "connexion":
                $_SESSION['menu'] = "connexion";
                return new ConnexionAction();
                break;
            case "deconnexion":
                $_SESSION['menu'] = "deconnexion";
                return new DeconnexionAction();
                break;

                //actions sur les Comptes
            case "formAjoutCompte":
                $_SESSION['menu'] = "comptes";
                return new AfficherFormAjoutCompte();
                break;
            case "listeUtilisateurs":
                $_SESSION['menu'] = "comptes";
                return new AfficherListeUtilisateursAction();
                break;
            case "ajouterCompte":
                $_SESSION['menu'] = "comptes";
                return new AjouterCompteAction();
                break;
            case "modifierCompte":
                $_SESSION['menu'] = "comptes";
                return new ModifierCompteAction();
                break;
            case "AfficherModifierCompte":
                $_SESSION['menu'] = "comptes";
                return new AfficherModifierCompteAction();
                break;
            case "confirmerSuppressionCompte":
                $_SESSION['menu'] = "comptes";
                return new ConfirmerSuppressionCompteAction();
                break;
            case "supprimerCompte":
                $_SESSION['menu'] = "comptes";
                return new SupprimerCompteAction();
                break;

                //Actions sur les Travaux
            case "AfficherAjoutTravail":
                $_SESSION['menu'] = "donnerTravail";
                return new AfficherAjoutTravailAction();
                break;
            case "AjoutTravail":
                $_SESSION['menu'] = "donnerTravail";
                return new AjoutTravailAction();
                break;
            case "afficherListeTaches":
                $_SESSION['menu'] = "afficherTravail";
                return new AfficherListeTaches();
                break;
            case "SupprimerTacheAction":
                $_SESSION['menu'] = "afficherTravail";
                return new SupprimerTacheAction();
                break;
            case "afficherTravail":
                $_SESSION['menu'] = "afficherTravail";
                return new AfficherTravailEleve();
                break;
            case "travail_fini":
                $_SESSION['menu'] = "travail_fini";
                return new TravailFiniAction();
                break;

                // Actions sur les messages
            case "formRedactionMessageProf":
                $_SESSION['menu'] = "memo";
                return new AfficherFormRedactionMessageProf();
                break;
            case "envoyerMessage":
                $_SESSION['menu'] = "memo";
                return new EnvoyerMessageAction();
                break;
            // Actions sur les vues du tableau de bord
            case "tableauBord" :
                $_SESSION['menu'] = "tableauBord";
                return new AfficherTableauBordAction();
                break;
            
            default:
                $_SESSION['menu'] = "default";
                return new DefaultAction();
        }
    }
}
