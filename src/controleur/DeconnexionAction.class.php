<?php

require_once('./controleur/Action.interface.php');

class DeconnexionAction implements Action
{
    public function execute()
    {
        if (isset($_SESSION)) {
            session_destroy();
        }

        $_REQUEST["info_type"] = "deconnexion";
        return "info";
    }
}
