<?php
/**
 * Reçoit l'action donnée par l'utilisateur et retourne la vue correspondante.
 *
 * @author Alex Duong et Yohan GK
 */

require_once('./controleur/Action.interface.php');
require_once('./modele/dao/CompteDAO.class.php');
require_once('./modele/classes/Compte.class.php');

class ModifierCompteAction implements Action
{
    public function execute()
    {
        $id_compte_connecte = $_SESSION["connecte"]["id"];
        $compte = CompteDAO::findByCourriel($_REQUEST['AncCourriel']);  //vérifier que l'encien courriel entré en correcte en même temps de créer l'objet compte

        if ($compte != null){
            //si l'utilisateur veux modifier son courriel
            if (ISSET($_REQUEST["ModifierCourriel"])) {
                if (!ISSET($_REQUEST["AncCourriel"]) || !ISSET($_REQUEST["ModCourriel"]) || !ISSET($_REQUEST["ConCourriel"])) { // Validation de la BD
                    $_REQUEST["message_erreur"] = "*Des paramètres sont manquants dans la requête. Veuillez contacter l'administrateur.";
                    return "modifierCompte";
                } elseif ($_REQUEST['AncCourriel'] == "" || $_REQUEST['ModCourriel'] == "" || $_REQUEST['ConCourriel'] == "") { // Validation pour les champs vides
                    $_REQUEST["message_erreur"] = "*Un ou plusieurs champs sont vides. Veuillez remplir tous les champs.";
                    return "modifierCompte";
                } else {
                    // Validation du nouveau courriel
                    if ($_REQUEST["ModCourriel"] != $_REQUEST["ConCourriel"]) {
                        $_REQUEST["message_erreur"] = "*Le nouveau courriel et sa confirmation doivent être identiques.";
                        return "modifierCompte";
                    }
                    //si la validation passe:
                    $compte->setCourriel($_REQUEST['ModCourriel']);
                }
            } else {
                $_REQUEST["message_erreur"] = "*Bouton non pesé!";
                return "modifierCompte";
            }

            CompteDAO::update($compte);
            
            // Message de confirmation de modification du compte
            $_REQUEST["message_confirmation"] = "&nbsp;Votre compte a été modifié avec succès!";
            return "modifierCompte";
        }
        // Validation de l'ancien courriel (si le compte == null)
        else {
            $_REQUEST["message_erreur"] = "*Votre ancien courriel est incorrect. Entrez à nouveau.";
            return "modifierCompte";
        }
    }
}
