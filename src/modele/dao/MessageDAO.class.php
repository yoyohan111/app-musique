<?php

include_once('./modele/classes/Database.class.php');
include_once('./modele/classes/Message.class.php');
include_once('./modele/classes/Liste.class.php');

class MessageDAO
{
    public static function find($id)
    {
        $db = Database::getInstance();

        $pstmt = $db->prepare("SELECT * FROM message WHERE id_message = :x");
        $pstmt->execute(array(':x' => $id));

        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $m = new Message();

        if ($result) {
            $m->setId_message($result->id_message);
            $m->setId_destinataire($result->id_destinataire);
            $m->setId_expediteur($result->id_expediteur);
            $m->setCategorie($result->categorie);
            $m->setEst_lu($result->est_lu);
            $m->setContenu($result->contenu);
            $m->setTitre($result->titre);
            $m->setDate_envoi($result->date_envoi);
            $pstmt->closeCursor();
            return $m;
        }
        $pstmt->closeCursor();
        return null;
    }

    public static function findAll()
    {
        try {
            $liste = new Liste();

            $requete = 'SELECT * FROM message';
            $cnx = Database::getInstance();

            $res = $cnx->query($requete);
            foreach ($res as $row) {
                $m = new Message();
                $m->loadFromArray($row);
                $liste->add($m);
            }
            $res->closeCursor();
            Database::close();
            $cnx = null;
            return $liste;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return $liste;
        }
    }

    public static function findAllByDestinataire($p_destinataire)
    {
        try {
            $liste = new Liste();
            $cnx = Database::getInstance();

            $pstmt = $cnx->prepare('SELECT * FROM message WHERE id_destinataire = :x');
            $pstmt->execute(array(':x' => $p_destinataire));

            $resultat = $pstmt->fetchAll(PDO::FETCH_NUM);

            foreach ($resultat as $row) {
                $m = new Message();
                $m->loadFromRecord($row);
                $liste->add($m);
            }

            $pstmt->closeCursor();

            $cnx = null;

            return $liste;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return $liste;
        }
    }

    public static function findAllByDestinataireDate($p_destinataire, $d)
    {
        try {
            $liste = new Liste();
            $cnx = Database::getInstance();

            $pstmt = $cnx->prepare('SELECT * FROM message WHERE id_destinataire = ? AND WEEK(date_envoi) >= WEEK(?)-1 AND WEEK(date_envoi) <= WEEK(?)');
            $pstmt->execute(array($p_destinataire, $d, $d));

            $resultat = $pstmt->fetchAll(PDO::FETCH_NUM);

            foreach ($resultat as $row) {
                $m = new Message();
                $m->loadFromRecord($row);
                $liste->add($m);
            }

            $pstmt->closeCursor();

            $cnx = null;

            return $liste;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return $liste;
        }
    }

    public function create($x)
    {
        try {
            $db = Database::getInstance();

            $id_message = $x->getId_message();
            $id_destinataire = $x->getId_destinataire();
            $id_expediteur = $x->getId_expediteur();
            $categorie = $x->getCategorie();
            $est_lu = $x->getEst_lu();
            $contenu = $x->getContenu();
            $titre = $x->getTitre();

            $requete = $db->prepare("INSERT INTO message (id_message, id_destinataire, id_expediteur, categorie, est_lu, contenu, titre, date_envoi) VALUES (:id_message, :id_destinataire, :id_expediteur, :categorie, :est_lu, :contenu, :titre, curdate())");
            $requete->bindParam(':id_message', $id_message);
            $requete->bindParam(':id_destinataire', $id_destinataire);
            $requete->bindParam(':id_expediteur', $id_expediteur);
            $requete->bindParam(':categorie', $categorie);
            $requete->bindParam(':est_lu', $est_lu);
            $requete->bindParam(':contenu', $contenu);
            $requete->bindParam(':titre', $titre);

            return $requete->execute();
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function update($x)
    {
        $request = "UPDATE message SET id_message = '" . $x->getId_message() . "', id_destinataire = '" . $x->getId_destinataire() . "', id_expediteur = '" . $x->getId_expediteur() . "', categorie = '" . $x->getCategorie() . "', est_lu = '" . $x->getEst_lu() . "', contenu = '" . $x->getContenu() . "', titre = '" . $x->getTitre() . "', date_envoi = '" . $x->getDate_envoi() . "'" .
            " WHERE id_message = '" . $x->getId_message() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function delete($x)
    {
        $request = "DELETE FROM message WHERE id_message = '" . $x->getId_message() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }
}
