<?php

include_once('./modele/classes/Database.class.php');
include_once('./modele/classes/Compte.class.php');
include_once('./modele/classes/ListeCompte.class.php');

/**
 * Objet d'accès aux données pour les comptes
 *
 * @author Alexandre Dupré, Alex Duong, Christopher Sarao
 */
class CompteDAO
{
    public static function find($id)
    {
        $db = Database::getInstance();

        $pstmt = $db->prepare("SELECT * FROM compte WHERE id_compte = :x");
        $pstmt->execute(array(':x' => $id));
        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new Compte();

        if ($result) {
            $p->setId($result->id_compte);
            $p->setNom($result->nom);
            $p->setCourriel($result->courriel);
            $p->setRole($result->role);
            $pstmt->closeCursor();
            return $p;
        }

        $pstmt->closeCursor();

        return null;
    }

    public static function findAll()
    {
        try {
            $liste = new ListeCompte();

            $requete = 'SELECT * FROM compte';
            $cnx = Database::getInstance();

            $res = $cnx->query($requete);
            foreach ($res as $row) {
                $c = new Compte();
                $c->loadFromRecord($row);
                $liste->add($c);
            }
            $res->closeCursor();
            $cnx = null;
            return $liste;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return $liste;  //retourne une liste d'objets Compte
        }
    }

    public static function findAllByRole($p_role)
    {
        try {
            $liste = new ListeCompte();
            $cnx = Database::getInstance();

            $pstmt = $cnx->prepare('SELECT * FROM compte WHERE role = :x ORDER BY nom');
            $pstmt->execute(array(':x' => $p_role));

            $resultat = $pstmt->fetchAll(PDO::FETCH_NUM);
            if (count($resultat) == 0) {
                return false;
            }

            foreach ($resultat as $row) {
                $c = new Compte();
                $c->loadFromRecord($row);
                $liste->add($c);
            }

            $pstmt->closeCursor();

            $cnx = null;
            return $liste;  //retourne une liste d'objets Compte
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return $liste;
        }
    }

    public function create($x)
    {
        $request = "INSERT INTO compte (ID_COMPTE ,NOM ,COURRIEL, ROLE)" .
            " VALUES ('" . $x->getId() . "','" . $x->getNom() . "','" . $x->getCourriel() . "','" . $x->getRole() . "')";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function update($x)
    {
        $request = "UPDATE compte SET NOM = '" . $x->getNom() . "', COURRIEL = '" . $x->getCourriel() . "', ROLE = '" . $x->getRole() . "'" .
            " WHERE ID_COMPTE = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function delete($x)
    {
        $request = "DELETE FROM compte WHERE ID_COMPTE = '" . $x->getId() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function getNbComptes()
    {
        try {
            $db = Database::getInstance();
            $request = $db->prepare("SELECT COUNT(*) FROM compte");
            $request->execute();
            return $request->fetchColumn();
        } catch (PDOException $e) {
            throw $e;
        }
    }

    public static function findByCourriel($courriel)
    {
        $db = Database::getInstance();
        $pstmt = $db->prepare("SELECT * FROM compte WHERE courriel = :x");
        $pstmt->execute(array(':x' => $courriel));

        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new Compte();

        if ($result) {
            $p->setId($result->id_compte);
            $p->setNom($result->nom);
            $p->setCourriel($result->courriel);
            $p->setRole($result->role);
            $pstmt->closeCursor();
            return $p;
        }
        $pstmt->closeCursor();
        return null;
    }

    public static function faire_relation_compte($id_prof, $id_eleve)
    {
        if ($id_prof == null || $id_eleve == null) {
            return false;
        } else {
            $request = "INSERT INTO relationcompte (id_prof, id_eleve)" .
                " VALUES ('" . $id_prof . "','" . $id_eleve . "')";
            try {
                $db = Database::getInstance();
                return $db->exec($request); //true pour la réussite de la requête
            } catch (PDOException $e) {
                throw $e;
            }
        }
    }

    public static function findAllRelationCompte($compte_id, $compte_role)
    {
        try {
            $liste = new ListeCompte();
            $cnx = Database::getInstance();

            if ($compte_role == 1) {
                $pstmt = $cnx->prepare("SELECT * FROM compte WHERE id_compte IN (SELECT id_eleve FROM relationcompte WHERE id_prof = '" . $compte_id . "')");
            } elseif ($compte_role == 2) {
                $pstmt = $cnx->prepare("SELECT * FROM compte WHERE id_compte IN (SELECT id_prof FROM relationcompte WHERE id_eleve = '" . $compte_id . "')");
            } else {
                return false;
            }
            
            if (!$pstmt->execute()) {
                return false;
            }

            $resultat = $pstmt->fetchAll(PDO::FETCH_NUM);

            if (count($resultat) == 0) {
                return false;
            }
            foreach ($resultat as $row) {
                $c = new Compte();
                $c->loadFromRecord($row);
                $liste->add($c);
            }

            $pstmt->closeCursor();

            $cnx = null;
            return $liste;  //retourne une liste d'objets Compte
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            return false;
        }
    }
}
