<?php
include_once($_SERVER['DOCUMENT_ROOT'] . '/app-musique/src/modele/classes/Database.class.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/app-musique/src/modele/classes/Tache.class.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/app-musique/src/modele/classes/StatutTacheEleve.class.php');
include_once($_SERVER['DOCUMENT_ROOT'] . '/app-musique/src/modele/classes/Compte.class.php');

class TacheDAO
{
    public static function find($id)
    {
        $db = Database::getInstance();

        $pstmt = $db->prepare("SELECT * FROM tache WHERE id_tache = :x");
        $pstmt->execute(array(':x' => $id));

        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new Tache();

        if ($result) {
            $p->setIdTache($result->id_tache);
            $p->setIdProf($result->id_prof);
            $p->setDateCreation($result->date_creation);
            $p->setNom($result->nom);
            $p->setDescription($result->description);
            $p->setInclureIteration($result->inclure_iteration);
            $p->setNomFichier($result->nom_fichier);
            $p->setTypeFichier($result->type_fichier);
            $p->setPathFichier($result->path_fichier);

            $pstmt->closeCursor();
            return $p;
        }
        $pstmt->closeCursor();
        return null;
    }
    
    public function create($x)
    {
        $request = "INSERT INTO tache (id_tache, id_prof, date_creation, nom, description, inclure_iteration, nom_fichier, type_fichier, path_fichier)" .
            " VALUES ('" . $x->getIdTache() . "','" . $x->getIdProf() . "','" . $x->getDateCreation() . "','" . $x->getNom() . "','" . $x->getDescription() . "','" . $x->getInclureIteration() . "','" . $x->getNomFichier() . "','" . $x->getTypeFichier() . "','" . $x->getPathFichier() . "')";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }
    
    public static function update($x)
    {
        $request = "UPDATE tache SET id_prof = '" . $x->getIdProf() . "', nom = '" . $x->getNom() . "', date_creation = '" . $x->getDateCreation() . "', description = '" . $x->getDescription() . "', inclure_iteration = '" . $x->getInclureIteration() . "',	est_termine = '" . "', nom_fichier = '" . $x->getNomFichier() . "', type_fichier = '" . $x->getTypeFichier() . "', path_fichier = '" . $x->getPathFichier() . "'" .
            " WHERE id_tache = '" . $x->getIdTache() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            throw $e;
        }
    }
    
    public static function delete($x)
    {
        $request = "DELETE FROM tache WHERE id_tache = '" . $x->getIdTache() . "'";
        try {
            $db = Database::getInstance();
            return $db->exec($request);
        } catch (PDOException $e) {
            return $e;
            throw $e;
        }
    }
    
    public static function deleteById($id)
    {
        $request_statut_tache = "DELETE FROM statut_tache_eleve WHERE id_tache = '" . $id . "'";    //delete de l'enfant en premier
        $request_tache = "DELETE FROM tache WHERE id_tache = '" . $id . "'";
        try {
            $db = Database::getInstance();
            $db->exec($request_statut_tache);
            $db->exec($request_tache);
            return true;
        } catch (PDOException $e) {
            return $e;
            throw $e;
        }
    }
    
    public static function FindAllForConnectedUser()
    {  //fonction qui retourne la liste des tâches selon l'utilisateur qui est connecté..
        try {
            $id_compte_connecte = $_SESSION["connecte"]["id"];
            $role_compte_connecte = $_SESSION["connecte"]["role"];

            $pdo = Database::getInstance();

            if ($role_compte_connecte == 2) { //si c'est un eleve qui est connecté
                $stmt = $pdo->query("SELECT * FROM tache WHERE id_tache IN (SELECT DISTINCT id_tache FROM statut_tache_eleve WHERE id_eleve = '" . $id_compte_connecte . "') ORDER BY date_creation DESC");
            } elseif ($role_compte_connecte == 1) {    //si c'est un Proffesseur qui est connecté
                $stmt = $pdo->query("SELECT * FROM tache WHERE id_prof = '" . $id_compte_connecte . "' ORDER BY date_creation DESC");
            } elseif ($role_compte_connecte == 0) {    //si c'est un Admin qui est connecté
                $stmt = $pdo->query("SELECT * FROM tache ORDER BY date_creation ORDER BY date_creation DESC");    //prendre tout les travaux de la base de donnée
            } else {
                echo "Le role de l'utilisateur connecté n'est pas bien défini!!!";
            } //DISTINCT dans la requete est pour enlever les doublons (car une tache peu être associé à plusieurs élèves, ce qui crée plusieurs relations avec le même id_tache).
            $liste_result = $stmt->fetchAll();

            if ($liste_result == false) {    //si la requête sql ne retourne rien
                return false;
            }
            $taches_object = array();
            foreach ($liste_result as $t) {
                $tache = new Tache();
                $tache->loadFromRecord($t);
                array_push($taches_object, $tache);
            }
            return $taches_object;
        } catch (Exception $e) {
            echo 'Caught exception in function FindAllForConnectedUser in TacheDAO: ',  $e->getMessage(), "\n";
            throw $e;
        }
    }
    
    public static function FindAllDate($d)
    {    //fonction qui retourne la liste des tâches de la semaine de la date $d, selon l'utilisateur qui est connecté
        try {
            $id_compte_connecte = $_SESSION["connecte"]["id"];
            $role_compte_connecte = $_SESSION["connecte"]["role"];

            $pdo = Database::getInstance();
            //faire les bonnes requêtes
            if ($role_compte_connecte == 2) { //si c'est un eleve qui est connecté
                $stmt = $pdo->query("SELECT * FROM tache WHERE id_tache IN (SELECT DISTINCT id_tache FROM statut_tache_eleve WHERE id_eleve = '" . $id_compte_connecte . "') AND WEEK(date_creation) >= WEEK('" . $d . "')-1 AND WEEK(date_creation) <= WEEK('" . $d . "') ORDER BY date_creation DESC");
            } elseif ($role_compte_connecte == 1) {    //si c'est un Proffesseur qui est connecté
                $stmt = $pdo->query("SELECT * FROM tache WHERE id_prof = '" . $id_compte_connecte . "' AND WEEK(date_creation) >= WEEK('" . $d . "')-1 AND WEEK(date_creation) <= WEEK('" . $d . "') ORDER BY date_creation DESC");
            } elseif ($role_compte_connecte == 0) {    //si c'est un Admin qui est connecté
                $stmt = $pdo->query("SELECT * FROM tache WHERE WEEK(date_creation) >= WEEK('" . $d . "')-1 AND WEEK(date_creation) <= WEEK('" . $d . "') ORDER BY date_creation DESC");
            } else {
                echo "Le role de l'utilisateur connecté n'est pas bien défini!!!";
            }

            if ($stmt == false) {
                return false;
            } else {
                $liste_result = $stmt->fetchAll();

                if ($liste_result == false) {    //si la requête sql ne retourne rien
                    return false;
                }
            }

            $taches_object = array();
            
            foreach ($liste_result as $t) {
                $tache = new Tache();
                $tache->loadFromRecord($t);
                array_push($taches_object, $tache);
            }

            return($taches_object);
        } catch (Exception $e) {
            throw $e;
        }
    }
    
    public static function faire_relation_eleves($id_tache, $liste_id_eleves)
    { //$id_eleves == array
        if ($liste_id_eleves == null) {
            return false;
        } elseif (gettype($liste_id_eleves) == "array") {    //$id_eleves est une liste contenant tout les ID des élèves assignés à la tâche
            for ($i = 0; $i < count($liste_id_eleves); $i++) {
                $request = "INSERT INTO statut_tache_eleve (id_tache, id_eleve)" .
                    " VALUES ('" . $id_tache . "','" . $liste_id_eleves[$i] . "')";
                try {
                    $db = Database::getInstance();
                    $db->exec($request);    //pas de return ici pour pouvoir rester dans la boucle
                } catch (PDOException $e) {
                    throw $e;
                    return false; //false pour l'échec de la requête
                }
            }
            return true;  //true pour la reéussite des requêtes
        } else {
            return false; //false si la requête n'a pas été effectué
        }
    }
    
    public static function get_statut_tache($id_tache, $id_eleve)
    {
        $db = Database::getInstance();

        $pstmt = $db->prepare("SELECT DISTINCT * FROM statut_tache_eleve WHERE id_tache = :x AND id_eleve = :y");
        $pstmt->execute(array(':x' => $id_tache, ':y' => $id_eleve));

        $result = $pstmt->fetch(PDO::FETCH_OBJ);
        $p = new StatutTacheEleve();

        if ($result) {
            $p->setIdTache($result->id_tache);
            $p->setIdEleve($result->id_eleve);
            $p->setEstTermine($result->est_termine);
            $p->setNbIteration($result->nb_iteration);

            $pstmt->closeCursor();
            return $p;
        }
        $pstmt->closeCursor();
        return null;
    }
    
    public static function update_statut_tache($x)
    {    //$id_eleves == array
        if ($x == null) {
            return false;
        } elseif (gettype($x) == "object") {    //$id_eleves est une liste contenant tout les ID des élèves assignés à la tâche
            $request = "UPDATE statut_tache_eleve SET est_termine = '" . $x->getEstTermine() . "', nb_iteration = '" . $x->getNbIteration() . "'" .
                " WHERE id_tache = '" . $x->getIdTache() . "' AND id_eleve = '" . $x->getIdEleve() . "'";
            try {
                $db = Database::getInstance();
                return $db->exec($request);
            } catch (PDOException $e) {
                throw $e;
            }
        }
        return true;  //true pour la reéussite des requêtes!
    }

    //TODO:
    public static function get_eleves_tache($t)
    {
        if ($t == null) {
            return false;
        } else {
            try {
                $pdo = Database::getInstance();
                $stmt = $pdo->query("SELECT c.nom, c.courriel, st.est_termine, st.nb_iteration FROM compte c INNER JOIN statut_tache_eleve st ON c.id_compte = st.id_eleve WHERE st.id_tache = '" . $t->getIdTache() . "'");
                $liste_result = $stmt->fetchAll();

                if ($liste_result == false) {    //si la requête sql ne retourne rien
                    return false;
                }
                return $liste_result;
            } catch (Exception $e) {
                echo 'Caught exception in function get_eleves_tache in TacheDAO: ',  $e->getMessage(), "\n";
                throw $e;
            }
        }
    }
}
