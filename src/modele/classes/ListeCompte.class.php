<?php

require_once('./modele/classes/Navigable.interface.php');

class ListeCompte implements Navigable {

    private $comptes;
    private $current = -1;

    public function __construct() { //Constructeur
        $this->comptes = array();
    }

    public function add($compte) {
        array_push($this->comptes, $compte);
    }

    public function previous() {
        if ($this->current > 0) {
            $this->current--;
            return true;
        }
        return false;
    }

    public function next() {
        if ($this->current < count($this->comptes)) {
            $this->current++;
            return true;
        }
        return false;
    }

    public function printCurrent() {
        if (isset($this->comptes[$this->current]))
            echo $this->comptes[$this->current];
    }

    public function getCurrent() {
        if (isset($this->comptes[$this->current]))
            return $this->comptes[$this->current];
        return null;
    }

    public function size() {
        return count($this->comptes);
    }

}

?>