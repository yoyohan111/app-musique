<?php

/**
 * Description of Compte
 *
 * @author Alexandre Dupré, Alex Duong
 */
class Compte
{
    private $id = "";
    private $nom = "Default";
    private $courriel = "";
    private $role = 2;

    public function __construct()
    {
        $this->id = uniqid();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getCourriel()
    {
        return $this->courriel;
    }

    public function getRole()
    {
        return $this->role;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function setCourriel($courriel)
    {
        $this->courriel = $courriel;
    }

    public function setRole($role)
    {
        $this->role = $role;
    }

    public function loadFromRecord($ligne)
    {
        $this->id = $ligne[0];
        $this->nom = $ligne[1];
        $this->courriel = $ligne[2];
        $this->role = $ligne[3];
    }

    public function __toString()
    {
        return "Compte[" . $this->id . "," . $this->nom . "," . $this->courriel . "," . $this->role . "]";
    }

    public function affiche()
    {
        echo $this->__toString();
    }
}
