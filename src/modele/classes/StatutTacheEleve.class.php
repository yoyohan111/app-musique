<?php

class StatutTacheEleve
{
    private $idTache;
    private $idEleve;
    private $estTermine;
    private $nbIteration;

    public function __construct()
    {
        $this->estTermine = 0; //0 pour false (par défaut)
        $this->nbIteration = 0;
    }

    //GETTERS
    public function getIdTache()
    {
        return $this->idTache;
    }

    public function getIdEleve()
    {
        return $this->idEleve;
    }

    public function getEstTermine()
    {
        return $this->estTermine;
    }

    public function getNbIteration()
    {
        return $this->nbIteration;
    }

    //SETTERS
    public function setIdTache($p)
    {
        $this->idTache = $p;
    }

    public function setIdEleve($p)
    {
        $this->idEleve = $p;
    }

    public function setEstTermine($p)
    {
        $this->estTermine = $p;
    }

    public function setNbIteration($p)
    {
        $this->nbIteration = $p;
    }

    //MÉTHODES
    public function iterationPlus()
    {
        $this->nbIteration++;
    }

    public function iterationMoins()
    {
        if ($this->nbIteration > 0) {   //ne peux pas être négatif
            $this->nbIteration--;
        }
    }

    public function loadFromRecord($ligne)
    {
        $this->idTache = $ligne[0];
        $this->idEleve = $ligne[1];
        $this->estTermine = $ligne[2];
        $this->nbIteration = $ligne[3];
    }

    public function __toString()
    {
        return "Tâche[" . $this->idTache . "," . $this->idEleve . "," . $this->estTermine . "," . $this->nbIteration . "]";
    }
}
