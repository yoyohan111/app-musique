<?php 


class Message 
{
    private $id_message = "";
    private $id_destinataire = "";
    private $id_expediteur = ""; 
    private $categorie = 2; 
    private $est_lu = false;
    private $contenu = "";
    private $titre = "";
    private $date_envoi = "";
    
    public function __construct()
    {
        $this->id_message = uniqid();
    }
    
    public function getId_message() {
        return $this->id_message;
    }

    public function getId_destinataire() {
        return $this->id_destinataire;
    }

    public function getId_expediteur() {
        return $this->id_expediteur;
    }

    public function getCategorie() {
        return $this->categorie;
    }

    public function getEst_lu() {
        return $this->est_lu;
    }

    public function getContenu() {
        return $this->contenu;
    }
    
    public function getTitre() {
        return $this->titre;
    }

    public function getDate_envoi() {
        return $this->date_envoi;
    }
    
    public function setId_message($id_message) {
        $this->id_message = $id_message;
    }

    public function setId_destinataire($id_destinataire) {
        $this->id_destinataire = $id_destinataire;
    }

    public function setId_expediteur($id_expediteur) {
        $this->id_expediteur = $id_expediteur;
    }

    public function setCategorie($categorie) {
        $this->categorie = $categorie;
    }

    public function setEst_lu($est_lu) {
        $this->est_lu = $est_lu;
    }

    public function setContenu($contenu) {
        $this->contenu = $contenu;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
    }

    public function setDate_envoi($date_envoi) {
        $this->date_envoi = $date_envoi;
    }
       
    public function loadFromRecord($ligne)
    {
        $this->id_message = $ligne[0];
        $this->id_destinataire = $ligne[1];
        $this->id_expediteur = $ligne[2];
        $this->categorie = $ligne[3];
        $this->est_lu = $ligne[4];
        $this->titre = $ligne[5];
        $this->contenu = $ligne[6];
        $this->date_envoi = $ligne[7];
    }
    
    public function __toString()
    {
        return "Message destiné à ".$this->id_destinataire." et envoyé le ".$this->getDate_envoi()." : ".$this->getContenu();
    }
}
?>
