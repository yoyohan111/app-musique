<?php

class Tache
{
    private $idTache;
    private $idProf;
    private $dateCreation;
    private $nom;
    private $description;
    private $inclureIteration;  //false par défaut
    private $nomFichier;
    private $typeFichier;
    private $pathFichier;

    public function __construct()
    {
        $this->idTache = uniqid();
        $this->inclureIteration = 0; //0 pour false (par défaut)
    }

    //GETTERS
    public function getIdTache()
    {
        return $this->idTache;
    }

    public function getIdProf()
    {
        return $this->idProf;
    }

    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getInclureIteration()
    {
        return $this->inclureIteration;
    }

    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    public function getTypeFichier()
    {
        return $this->typeFichier;
    }

    public function getPathFichier()
    {
        return $this->pathFichier;
    }

    //SETTERS
    public function setIdTache($p)
    {
        $this->idTache = $p;
    }

    public function setIdProf($p)
    {
        $this->idProf = $p;
    }

    public function setDateCreation($p)
    {
        $this->dateCreation = $p;
    }

    public function setNom($p)
    {
        $this->nom = $p;
    }

    public function setDescription($p)
    {
        $this->description = $p;
    }

    public function setInclureIteration($p)
    {
        $this->inclureIteration = $p;
    }

    public function setNomFichier($p)
    {
        $this->nomFichier = $p;
    }

    public function setTypeFichier($p)
    {
        $this->typeFichier = $p;
    }

    public function setPathFichier($p)
    {
        $this->pathFichier = $p;
    }

    public function loadFromRecord($ligne)
    {
        $this->idTache = $ligne[0];
        $this->idProf = $ligne[1];
        $this->dateCreation = $ligne[2];
        $this->nom = $ligne[3];
        $this->description = $ligne[4];
        $this->inclureIteration = $ligne[5];
        $this->nomFichier = $ligne[6];
        $this->typeFichier = $ligne[7];
        $this->pathFichier = $ligne[8];
    }

    public function __toString()
    {
        return "Tâche[" . $this->idTache . "," . $this->idProf . "," . $this->dateCreation . "," . $this->nom . "," . $this->description . ","
            . $this->inclureIteration . "," . $this->nomFichier . "," . $this->typeFichier . "," . $this->pathFichier . "]";
    }
}
