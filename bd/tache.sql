SET time_zone = 'SYSTEM';

CREATE TABLE IF NOT EXISTS `tache` (
  `id_tache` varchar(13) NOT NULL,
  `id_prof` varchar(13),
  `date_creation` DATE NOT NULL,
  `nom` varchar(255) NOT NULL,
  `description` varchar(1024),
  `inclure_iteration` BOOLEAN NOT NULL DEFAULT 0,
  `nom_fichier` varchar(255),
  `type_fichier` varchar(255),
  `path_fichier` varchar(255),
-- Contraintes
    PRIMARY KEY (`id_tache`),
    FOREIGN KEY (id_prof) REFERENCES compte(id_compte) ON DELETE SET NULL
)DEFAULT CHARSET=utf8;