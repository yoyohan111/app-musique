CREATE TABLE IF NOT EXISTS relationcompte (
    id_prof varchar(13) NOT NULL,
    id_eleve varchar(13) NOT NULL,
    type SMALLINT(1) DEFAULT NULL,
-- Contraintes
    PRIMARY KEY (id_prof, id_eleve),
    FOREIGN KEY (id_prof) REFERENCES compte(id_compte) ON DELETE CASCADE,
    FOREIGN KEY (id_eleve) REFERENCES compte(id_compte) ON DELETE CASCADE
)DEFAULT CHARSET=utf8;