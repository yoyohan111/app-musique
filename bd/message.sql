CREATE TABLE IF NOT EXISTS message (
    id_message varchar(13) NOT NULL,
    id_destinataire varchar(13) NOT NULL,
    id_expediteur varchar(13) NOT NULL,
    categorie smallint(1) NOT NULL, -- 0 : Important | 1 : Absence | 2 : Générique
    est_lu boolean NOT NULL,
    titre varchar(255) NOT NULL,
    contenu varchar(255) NOT NULL,
    date_envoi DATE NOT NULL,
-- Contraintes
    PRIMARY KEY (id_message),
    FOREIGN KEY (id_destinataire) REFERENCES compte(id_compte) ON DELETE CASCADE,
    FOREIGN KEY (id_expediteur) REFERENCES compte(id_compte) ON DELETE CASCADE
)DEFAULT CHARSET=utf8;
