CREATE TABLE IF NOT EXISTS compte(
    id_compte varchar(13) NOT NULL,
    nom varchar(255) NOT NULL,
    courriel varchar(255) NOT NULL UNIQUE,
    role tinyint(1) NOT NULL,   -- 0 = Administrateur, 1 = Professeur, 2 = Étudiant
-- Contraintes
    PRIMARY KEY (id_compte)
)DEFAULT CHARSET=utf8;
