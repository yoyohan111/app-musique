-- Suppression des tables avec des relations
DROP TABLE IF EXISTS statut_tache_eleve;
DROP TABLE IF EXISTS relationcompte;
DROP TABLE IF EXISTS message;
-- Suppression des tables des classes entités
DROP TABLE IF EXISTS compte;
DROP TABLE IF EXISTS tache;