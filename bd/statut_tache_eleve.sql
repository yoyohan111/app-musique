CREATE TABLE IF NOT EXISTS `statut_tache_eleve` (
    `id_tache` varchar(13) NOT NULL,
    `id_eleve` varchar(13) NOT NULL,
    `est_termine` BOOLEAN NOT NULL DEFAULT 0,
    `nb_iteration` int(3) NOT NULL DEFAULT 0,
-- Contraintes
    PRIMARY KEY (id_tache, id_eleve),
    FOREIGN KEY (id_tache) REFERENCES tache(id_tache) ON DELETE CASCADE,
    FOREIGN KEY (id_eleve) REFERENCES compte(id_compte) ON DELETE CASCADE
)DEFAULT CHARSET=utf8;