# Application Musique

## Pour la première utilisation:
  0. changer le encoding de la base de donnée à `utf8_general_ci`
  1. Importer la base de donnée dans le bonne ordre indiqué dans le fichier `LISEZ-MOI.txt` du dossier `bd`
  2. Changer le fichier `src/modele/configs/config.php` pour configurer et relier la base de donnée avec le serveur sql
  3. Créer un Admin manuellement (créer un compte prof ou étudiant avec le lien `inscription`, puis changer son rôle dans la base de donnée à `0`, cela le rendera un Administrateur.
  4. Connectez-vous avec l'admin pour créer des comptes Prof et Étudiants comme désiré.
  5. Réjouissez! :)


## Pour contacter le client :

  - Courriel : agagnier@professeur-musique.com

## Dossiers :

- **bd : musique : Contient les fichiers SQL.**

***Importer avec PHPMyAdmin dans cet ordre :****

    - suppression_tables.sql
    - compte.sql
    - tache.sql
    - message.sql
    - statut_tache_eleve.sql
    - relationcompte.sql
    

  - **src : Contient le code source de l'application.** Copier ce dossier dans
    Uwamp au besoin.
    - controleur : Contient les actions de l'application.
    - modele : Contient les classes de l'application.
    - vues : Contient les pages (que l'utilisateur voit) de l'application.
    - styles : Tous les fichiers CSS, Javascript et images sont dans ce dossier.
<br />

  - **tests : Contient tout les tests unitaires avec PHPUnit des classes pour l'instant**
